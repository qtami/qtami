/*
 * Copyright © 2011-2012 CarelessChaser <chaser.andrey@gmail.com>
 *
 * This file is part of QtAmi
 *
 * QtAmi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QtAmi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QtAmi. If not, see <http://www.gnu.org/licenses/>.
 */
#include "qamievent.h"
#include <sys/syslog.h>

using namespace QAmi;

QAmiEvent::QAmiEvent(const QByteArray &str) :
    QAmiPropertyMap(str)
{
    const QString type = value(KeyEvent);
    if(!EventStrTypes.contains(type)) {
        syslog(LOG_WARNING, "AMI: No defined event type: %s", qPrintable(type));
        syslog(LOG_WARNING, "EVENT: '%s'", str.constData());
        qWarning("AMI: No defined event type: %s", qPrintable(type));
        qDebug( "EVENT: '%s'", str.constData() );
    }
    m_Type = EventStrTypes[type];
}

QAmiEvent::QAmiEvent(const QAmiEvent &ev) :
    QAmiPropertyMap()
{
    setValues(ev.values());
    m_Type = ev.type();
}

QAmiEvent::~QAmiEvent()
{
}

QString QAmiEvent::actionId() const
{
    return value("ActionId");
}

QString QAmiEvent::typeStr() const
{
    return value(KeyEvent);
}

EventType QAmiEvent::type() const
{
    return m_Type;
}


