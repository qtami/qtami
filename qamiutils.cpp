/*
 * Copyright © 2011-2012 CarelessChaser <chaser.andrey@gmail.com>
 *
 * This file is part of QtAmi
 *
 * QtAmi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QtAmi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QtAmi. If not, see <http://www.gnu.org/licenses/>.
 */
#include "qamiutils.h"

using namespace QAmi;

QString QAmi::str2log(QString str)
{
    return str.replace('\r', "\\r").replace('\n', "\\n");
}

bool QAmi::isChannelState(int x)
{
    switch (static_cast<AstChannelState>(x)) {
    case AstStateDown:
    case AstStateReserved:
    case AstStateOffhook:
    case AstStateDialing:
    case AstStateRing:
    case AstStateUp:
    case AstStateBusy:
    case AstStateDialingOffhook:
    case AstStatePrering:
    case AstStateMute:
        return true;
    default:
        return false;
    }
}

bool QAmi::isDeviceState(int x)
{
    switch (static_cast<AstDeviceState>(x)) {
    case AstDeviceUnknown:
    case AstDeviceNotInUse:
    case AstDeviceInUse:
    case AstDeviceBusy:
    case AstDeviceInvalid:
    case AstDeviceUnavailable:
    case AstDeviceRinging:
    case AstDeviceRingInUse:
    case AstDeviceOnHold:
    case AstDeviceTotal:
        return true;
    default:
        return false;
    }
}
