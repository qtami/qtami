/*
 * Copyright © 2011-2012 CarelessChaser <chaser.andrey@gmail.com>
 *
 * This file is part of QtAmi
 *
 * QtAmi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QtAmi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QtAmi. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef QAMIREADER_H
#define QAMIREADER_H

#include <QtCore/QSharedPointer>
#include <QtCore/QString>
#include <QtCore/QThread>
#include <QtCore/QTimer>
#include <QtNetwork/QTcpSocket>

namespace QAmi {

class QAmiReader : public QObject
{
    Q_OBJECT
public:
    explicit QAmiReader(QObject *parent = 0);
    ~QAmiReader();
    void stop();
    void close();
    void setSocketDescriptor(int socketDescriptor);


private:

    QByteArray m_Buffer;
    QTcpSocket m_Socket;
    QTimer m_Timer;
    bool m_Autoreconnect;

    void processIncoming(const QByteArray &strNew);
signals:
    void newEvent(QByteArray ev);
    void newResponse(QByteArray response);
    void newVersion(QByteArray version);
    void connected();
    void disconnected();
    void debugReceived(QByteArray);
    void finished();

public slots:
    void disconnectFromHost();
    void sendAction(QString str);
    void connectToHost(const QString & host, const quint16 port);
private slots:
    void socketRead();
    void socketConnected();
    void socketDisconnected();
    void socketError(QAbstractSocket::SocketError error);
    void onSocketStateChanged(QAbstractSocket::SocketState state);
    void onSocketDestroyed(QObject *obj);
};

}

#endif // QAMIREADER_H
