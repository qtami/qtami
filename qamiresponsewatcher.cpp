/*
 * Copyright © 2011-2012 CarelessChaser <chaser.andrey@gmail.com>
 *
 * This file is part of QtAmi
 *
 * QtAmi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QtAmi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QtAmi. If not, see <http://www.gnu.org/licenses/>.
 */
#include "qamiresponsewatcher.h"

using namespace QAmi;

QAmiResponseWatcher::QAmiResponseWatcher(QObject *parent) :
    QObject(parent)
{
}

QAmiResponseWatcher::QAmiResponseWatcher(const QAmiAction &action, QObject *parent) :
    QObject(parent)
{
    mAction = action;
}

void QAmiResponseWatcher::trigger()
{
    emit triggered();
}

QAmiAction QAmiResponseWatcher::action()
{
    return mAction;
}

QString QAmiResponseWatcher::actionId()
{
    return mAction.actionId();
}

void QAmiResponseWatcher::addEvent(QAmiEvent ev)
{
    mEvents.append(ev);
}

const QAmiEvent QAmiResponseWatcher::lastEvent()
{
    return mEvents.last();
}

const QList<QAmiEvent>& QAmiResponseWatcher::events()
{
    return mEvents;
}

void QAmiResponseWatcher::setType(Type type)
{
    m_Type = type;
}

QAmiResponseWatcher::Type QAmiResponseWatcher::type()
{
    return m_Type;
}

