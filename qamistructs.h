/*
 * Copyright © 2011-2012 CarelessChaser <chaser.andrey@gmail.com>
 *
 * This file is part of QtAmi
 *
 * QtAmi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QtAmi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QtAmi. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef QAMISTRUCTS_H
#define QAMISTRUCTS_H

#include <QtCore/QHash>
#include <QtCore/QMap>

namespace QAmi {

enum ActionType {
    AcCommand,
    AcCoreShowChannels,
    AcDongleReset,
    AcDongleSendSMS,
    AcDongleSendUSSD,
    AcDongleShowDevices,
    AcGetvar,
    AcHangup,
    AcListCommands,
    AcLogin,
    AcOriginate,
    AcPing,
    AcSetvar,
    AcSippeers,
    AcSIPshowpeer,
    AcStatus,
    AcQueueAdd,
    AcQueuePause,
    AcQueueRemove,
    AcQueueStatus
};

enum EventType {
    EvTypeAGIExec,
    EvTypeAgentCalled,
    EvTypeAgentComplete,
    EvTypeAgentConnect,
    EvTypeAgentRingNoAnswer,
    EvTypeBridge,
    EvTypeChannelReload,
    EvTypeChannelUpdate,
    EvTypeCoreShowChannel,
    EvTypeCoreShowChannelsComplete,
    EvTypeDial,
    EvTypeDongleCallStateChange,
    EvTypeDongleCEND,
    EvTypeDongleDeviceEntry,
    EvTypeDongleNewCMGR,
    EvTypeDongleNewCUSD,
    EvTypeDongleNewSMS,
    EvTypeDongleNewSMSBase64,
    EvTypeDongleNewUSSD,
    EvTypeDongleNewUSSDBase64,
    EvTypeDonglePortFail,
    EvTypeDongleShowDevicesComplete,
    EvTypeDongleSMSStatus,
    EvTypeDongleStatus,
    EvTypeDongleUSSDStatus,
    EvTypeDTMF,
    EvTypeFullyBooted,
    EvTypeHangup,
    EvTypeJoin,
    EvTypeLeave,
    EvTypeMasquerade,
    EvTypeMusicOnHold,
    EvTypeNewAccountCode,
    EvTypeNewCallerid,
    EvTypeNewchannel,
    EvTypeNewexten,
    EvTypeNewstate,
    EvTypePeerEntry,
    EvTypePeerStatus,
    EvTypeQueueCallerAbandon,
    EvTypeQueueEntry,
    EvTypeQueueMember,
    EvTypeQueueMemberAdded,
    EvTypeQueueMemberPaused,
    EvTypeQueueMemberRemoved,
    EvTypeQueueMemberStatus,
    EvTypeQueueParams,
    EvTypeQueueStatusComplete,
    EvTypeRegistry,
    EvTypeRename,
    EvTypeRTCPReceived,
    EvTypeRTCPSent,
    EvTypeShutdown,
    EvTypeStatus,
    EvTypeStatusComplete,
    EvTypeTransfer,
    EvTypeUnlink,
    EvTypeVarSet
};

/*!
  Допустимые ключи, которые могут присутствовать в Action/Event/Response
 */

enum AmiKey {
    KeyAbandoned,
    KeyAccount,
    KeyAccountCode,
    KeyAccountcode,
    KeyACL,
    KeyActionID,
    KeyActive,
    KeyAddress,
    KeyAgentName,
    KeyAlerting,
    KeyAppData,
    KeyApplication,
    KeyApplicationData,
    KeyAsync,
    KeyAudioSetting,
    KeyAudioState,
    KeyAutoDeleteSMS,
    KeyBegin,
    KeyBridgedChannel,
    KeyBridgedUniqueID,
    KeyBridgestate,
    KeyBridgetype,
    KeyCallerid,
    KeyCallerID1,
    KeyCallerID2,
    KeyCallerIDName,
    KeyCallerIDname,
    KeyCallerIDNum,
    KeyCallerIDnum,
    KeyCallIdx,
    KeyCalls,
    KeyCallsChannels,
    KeyCallsTaken,
    KeyCallWaitingSetting,
    KeyCallWaitingState,
    KeyCause,
    KeyCauseTxt,
    KeyCCCause,
    KeyCellID,
    KeyChannel,
    KeyChannel1,
    KeyChannel2,
    KeyChannelLanguage,
    KeyChannelState,
    KeyChannelStateDesc,
    KeyChannelType,
    KeyChanneltype,
    KeyChanObjectType,
    KeyCID_CallingPres,
    KeyClone,
    KeyCloneState,
    KeyCommand,
    KeyCommandId,
    KeyCommandsInQueue,
    KeyCompleted,
    KeyConnectedLineName,
    KeyConnectedLineNum,
    KeyContext,
    KeyCount,
    KeyCumulativeLoss,
    KeyCurrentDeviceState,
    KeyDataSetting,
    KeyDataState,
    KeyDefaultCallingPres,
    KeyDesiredDeviceState,
    KeyDestination,
    KeyDestUniqueID,
    KeyDevice,
    KeyDialing,
    KeyDialstring,
    KeyDigit,
    KeyDirection,
    KeyDisableSMS,
    KeyDLSR,
    KeyDomain,
    KeyDTMF,
    KeyDynamic,
    KeyDuration,
    KeyEnd,
    KeyEndStatus,
    KeyEvent,
    KeyEventList,
    KeyExten,
    KeyExtension,
    KeyFirmware,
    KeyForcerport,
    KeyFractionLost,
    KeyFrom,
    KeyGroup,
    KeyGSMRegistrationStatus,
    KeyHeld,
    KeyHighestSequence,
    KeyHoldTime,
    KeyHoldtime,
    KeyIAJitter,
    KeyID,
    KeyIMEISetting,
    KeyIMEIState,
    KeyIMSISetting,
    KeyIMSIState,
    KeyIncoming,
    KeyInitializing,
    KeyInterface,
    KeyIPaddress,
    KeyIPport,
    KeyItems,
    KeyLastCall,
    KeyLastSR,
    KeyListItems,
    KeyLocation,
    KeyLocationAreaCode,
    KeyManufacturer,
    KeyMax,
    KeyMemberName,
    KeyMembership,
    KeyMessage,
    KeyMinimalDTMFDuration,
    KeyMinimalDTMFGap,
    KeyMinimalDTMFInterval,
    KeyMode,
    KeyModel,
    KeyName,
    KeyNewname,
    KeyNewState,
    KeyNumber,
    KeyObjectName,
    KeyOldAccountCode,
    KeyOriginal,
    KeyOriginalPosition,
    KeyOriginalState,
    KeyOurSSRC,
    KeyPacketsLost,
    KeyPaused,
    KeyPeer,
    KeyPeer_Count,
    KeyPeerStatus,
    KeyPenalty,
    KeyPosition,
    KeyPriority,
    KeyPrivilege,
    KeyProviderName,
    KeyPT,
    KeyQueue,
    KeyRealtimeDevice,
    KeyReason,
    KeyReceptionReports,
    KeyRegistry_Count,
    KeyReleasing,
    KeyReloadReason,
    KeyReportBlock,
    KeyResetDongle,
    KeyResult,
    KeyResultCode,
    KeyRSSI,
    KeyRTT,
    KeyRXGain,
    KeySeconds,
    KeySecret,
    KeySenderSSRC,
    KeySentNTP,
    KeySentOctets,
    KeySentPackets,
    KeySentRTP,
    KeySequenceNumberCycles,
    KeyServiceLevel,
    KeyServicelevelPerf,
    KeySIP_Callid,
    KeySMS,
    KeySMSPDU,
    KeySMSServiceCenter,
    KeyState,
    KeyStatus,
    KeyStrategy,
    KeySubEvent,
    KeySubmode,
    KeySubscriberNumber,
    KeyTalkTime,
    KeyTargetChannel,
    KeyTargetUniqueid,
    KeyTasksInQueue,
    KeyTextSupport,
    KeyTimestamp,
    KeyTheirLastSR,
    KeyTo,
    KeyTransferContext,
    KeyTransferExten,
    KeyTransferMethod,
    KeyTransferType,
    KeyTXGain,
    KeyU2DIAG,
    KeyUniqueID,
    KeyUniqueid,
    KeyUniqueid1,
    KeyUniqueid2,
    KeyUseCallingPres,
    KeyUseUCS2Encoding,
    KeyUSSD,
    KeyUSSDUse7BitEncoding,
    KeyUSSDUseUCS2Decoding,
    KeyUsername,
    KeyValue,
    KeyVariable,
    KeyVideoSupport,
    KeyVoice,
    KeyWaiting,
    KeyWeight
};

enum AstDeviceState {
    AstDeviceUnknown,       /*!< Device is valid but channel didn't know state */
    AstDeviceNotInUse,      /*!< Device is not used */
    AstDeviceInUse,         /*!< Device is in use */
    AstDeviceBusy,          /*!< Device is busy */
    AstDeviceInvalid,       /*!< Device is invalid */
    AstDeviceUnavailable,   /*!< Device is unavailable */
    AstDeviceRinging,       /*!< Device is ringing */
    AstDeviceRingInUse,     /*!< Device is ringing *and* in use */
    AstDeviceOnHold,        /*!< Device is on hold */
    AstDeviceTotal          /*/ Total num of device states, used for testing */
};


enum AstChannelState {
    AstStateDown,           /*!< Channel is down and available */
    AstStateReserved,       /*!< Channel is down, but reserved */
    AstStateOffhook,        /*!< Channel is off hook */
    AstStateDialing,        /*!< Digits (or equivalent) have been dialed */
    AstStateRing,           /*!< Line is ringing */
    AstStateRinging,        /*!< Remote end is ringing */
    AstStateUp,             /*!< Line is up */
    AstStateBusy,           /*!< Line is busy */
    AstStateDialingOffhook, /*!< Digits (or equivalent) have been dialed while offhook */
    AstStatePrering,        /*!< Channel has detected an incoming call and is waiting for ring */

    AstStateMute = (1 << 16)   /*!< Do not transmit voice data */
};

enum PeerStatus {
    PeerStatusRegistered,
    PeerStatusReachable,
    PeerStatusUnregistered,
    PeerStatusUnreachable
};

extern const QHash<ActionType, QString> ActionTypes;

extern const QMap<QString, ActionType> ActionStrTypes;

extern const QHash<EventType, QString> EventTypes;

extern const QMap<QString, EventType> EventStrTypes;

extern const QHash<AmiKey, QString> AmiKeys;

extern const QMap<QString, AmiKey> AmiStrKeys;

extern const QMap<QString, AstChannelState> AstChannelStrStates;

}

#endif // QAMISTRUCTS_H
