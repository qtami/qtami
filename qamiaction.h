/*
 * Copyright © 2011-2012 CarelessChaser <chaser.andrey@gmail.com>
 *
 * This file is part of QtAmi
 *
 * QtAmi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QtAmi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QtAmi. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef QAMIABSTRACTACTION_H
#define QAMIABSTRACTACTION_H

#include <QtCore/QString>


#include "qamipropertymap.h"

namespace QAmi {

class QAmiAction : public QAmiPropertyMap
{
public:
    QAmiAction();
    QAmiAction(ActionType type);
    QAmiAction(const QAmiAction &action);
    virtual ~QAmiAction();

    virtual QString name() const;
    virtual QString toString() const;

    QString actionId() const;
    ActionType type() const;
    void setActionType(ActionType type);

protected:
    void setActionId(const QString &str);
    const QString generateId();
private:
    ActionType mType;
};

class ActionGetvar : public QAmiAction
{
public:
    ActionGetvar(const QAmiAction &action) : QAmiAction(action) { setActionType(QAmi::AcGetvar); }
    ActionGetvar() { setActionType(QAmi::AcGetvar); }
    inline QString channel() const { return value(KeyChannel); }
    inline QString variable() const { return value(KeyVariable); }
    inline void setChannel(const QString &channel) { setValue(KeyChannel, channel); }
    inline void setVariable(const QString &variable) { setValue(KeyVariable, variable); }
};

class ActionDongleReset : public QAmiAction
{
public:
    ActionDongleReset(const QAmiAction &action) : QAmiAction(action) { setActionType(QAmi::AcDongleReset); }
    ActionDongleReset() { setActionType(QAmi::AcDongleReset); }
    inline void setDevice(const QString &device) { setValue(KeyDevice, device); }
};

class ActionDongleSendSMS : public QAmiAction
{
public:
    ActionDongleSendSMS(const QAmiAction &action) : QAmiAction(action) { setActionType(QAmi::AcDongleSendSMS); }
    ActionDongleSendSMS() { setActionType(QAmi::AcDongleSendSMS); }
    inline QString device() const { return value(KeyDevice); }
    inline QString message() const { return value(KeyMessage); }
    inline QString number() const { return value(KeyNumber); }
    inline void setDevice(const QString &device) { setValue(KeyDevice, device); }
    inline void setNumber(const QString &number) { setValue(KeyNumber, number); }
    inline void setMessage(const QString &message) { setValue(KeyMessage, message); }
};

class ActionDongleSendUSSD : public QAmiAction
{
public:
    ActionDongleSendUSSD() { setActionType(QAmi::AcDongleSendUSSD); }
    ActionDongleSendUSSD(const QAmiAction &action) : QAmiAction(action) { setActionType(QAmi::AcDongleSendSMS); }
    inline void setDevice(const QString &device) { setValue(KeyDevice, device); }
    inline void setUssd(const QString &ussd) { setValue(KeyUSSD, ussd); }
};

class ActionHangup : public QAmiAction
{
public:
    ActionHangup() { setActionType(QAmi::AcHangup); }
    inline void setChannel(const QString &channel) { setValue(KeyChannel, channel); }
};

class ActionLogin : public QAmiAction
{
public:
    ActionLogin() { setActionType(AcLogin); }
    inline void setUsername(const QString &username) { setValue(KeyUsername, username); }
    inline void setSecret(const QString &secret) { setValue(KeySecret, secret); }
};

class ActionOriginate : public QAmiAction
{
public:
    ActionOriginate() { setActionType(QAmi::AcOriginate); }
    ActionOriginate(const QAmiAction &action) : QAmiAction(action) { setActionType(QAmi::AcOriginate); }
    inline QString channel() const { return value(KeyChannel); }
    inline QString exten() const { return value(KeyExten); }
    inline void setAsync(const QString &async) { setValue(KeyAsync, async); }
    inline void setCallerid(const QString &callerid) { setValue(KeyCallerid, callerid); }
    inline void setChannel(const QString &channel) { setValue(KeyChannel, channel); }
    inline void setContext(const QString &context) { setValue(KeyContext, context); }
    inline void setExten(const QString &exten) { setValue(KeyExten, exten); }
    inline void setPriority(const QString &priority) { setValue(KeyPriority, priority); }
    inline void setVariable(const QString &variable) { setMultiValue(KeyVariable, variable); }
};

class ActionQueueAdd : public QAmiAction
{
public:
    ActionQueueAdd() { setActionType(QAmi::AcQueueAdd); }
    inline void setInterface(const QString &interface) { setValue(KeyInterface, interface); }
    inline void setMemberName(const QString &memberName) { setValue(KeyMemberName, memberName); }
    inline void setPaused(const QString &paused) { setValue(KeyPaused, paused); }
    inline void setPenalty(const QString &penalty) { setValue(KeyPenalty, penalty); }
    inline void setQueue(const QString &queue) { setValue(KeyQueue, queue); }
};

class ActionQueuePause : public QAmiAction
{
public:
    ActionQueuePause() { setActionType(QAmi::AcQueuePause); }
    ActionQueuePause(const QAmiAction &action) : QAmiAction(action) { setActionType(QAmi::AcQueuePause); }
    inline QString interface() const { return value(KeyInterface); }
    inline QString reason() const { return value(KeyReason); }
    inline void setInterface(const QString &interface) { setValue(KeyInterface, interface); }
    inline void setPaused(const QString &paused) { setValue(KeyPaused, paused); }
    inline void setReason(const QString &reason) { setValue(KeyReason, reason); }
};

class ActionQueueRemove : public QAmiAction
{
public:
    ActionQueueRemove() { setActionType(QAmi::AcQueueRemove); }
    inline void setQueue(const QString &queue) { setValue(KeyQueue, queue); }
    inline void setInterface(const QString &interface) { setValue(KeyInterface, interface); }
};

class ActionSetvar : public QAmiAction
{
public:
    ActionSetvar() { setActionType(QAmi::AcSetvar); }
    ActionSetvar(const QAmiAction &action) : QAmiAction(action) { setActionType(QAmi::AcSetvar); }
    inline void setValue(const QString &value) { QAmiAction::setValue(KeyValue, value); }
    inline void setVariable(const QString &variable) { QAmiAction::setValue(KeyVariable, variable); }
};

}

#endif // QAMIABSTRACTACTION_H
