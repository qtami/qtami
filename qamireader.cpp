/*
 * Copyright © 2011-2012 CarelessChaser <chaser.andrey@gmail.com>
 *
 * This file is part of QtAmi
 *
 * QtAmi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QtAmi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QtAmi. If not, see <http://www.gnu.org/licenses/>.
 */
#include <sys/syslog.h>
#include "qamireader.h"


const char SEP[] = "\r\n\r\n";
const QByteArray SEPLN = "\r\n";

using namespace QAmi;

QAmiReader::QAmiReader(QObject *parent) :
    QObject(parent),
    m_Socket(this),
    m_Autoreconnect(true)
{
    connect(&m_Socket, SIGNAL(readyRead()), SLOT(socketRead()));
    connect(&m_Socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), SLOT(onSocketStateChanged(QAbstractSocket::SocketState)));
    connect(&m_Socket, SIGNAL(destroyed(QObject*)), SLOT(onSocketDestroyed(QObject*)));
    connect(&m_Socket, SIGNAL(connected()), SLOT(socketConnected()));
    connect(&m_Socket, SIGNAL(disconnected()), SLOT(socketDisconnected()));
    connect(&m_Socket, SIGNAL(error(QAbstractSocket::SocketError)), SLOT(socketError(QAbstractSocket::SocketError)));
}


QAmiReader::~QAmiReader()
{
    QAmiReader::close();
}

void QAmiReader::connectToHost(const QString &host, const quint16 port)
{
    m_Socket.connectToHost(host, port);
}

void QAmiReader::stop()
{
    m_Socket.disconnectFromHost();
}

void QAmiReader::socketConnected()
{
    emit connected();
}

void QAmiReader::socketDisconnected()
{
    m_Buffer.clear();
    emit disconnected();
}

void QAmiReader::socketError(QAbstractSocket::SocketError error)
{
    syslog(LOG_ERR, "%s: Socket Error %d: %s", Q_FUNC_INFO, error, qPrintable(m_Socket.errorString()));
    qCritical( "%s: Socket Error %d: %s", Q_FUNC_INFO, error, qPrintable(m_Socket.errorString()) );
}

void QAmiReader::onSocketStateChanged(QAbstractSocket::SocketState state)
{
    syslog(LOG_DEBUG, "%s: State changed to %d", Q_FUNC_INFO, state);
    qDebug("%s: State changed to %d", Q_FUNC_INFO, state);
    if(state == QAbstractSocket::UnconnectedState)
        socketDisconnected();
}

void QAmiReader::onSocketDestroyed(QObject *obj)
{
    syslog(LOG_WARNING, "%s: %p", Q_FUNC_INFO, (void*)obj);
    qDebug("%s: %p", Q_FUNC_INFO, (void*)obj);
}

void QAmiReader::close()
{
    m_Socket.close();
}

void QAmiReader::disconnectFromHost()
{
    m_Socket.disconnectFromHost();
    emit finished();
}

void QAmiReader::processIncoming(const QByteArray &strNew)
{
    QByteArray str(m_Buffer + strNew);
    m_Buffer.clear();
    int cutAt = 0;
    int endAt = 0;

    while((cutAt = str.indexOf(SEPLN, cutAt)) != -1) {
        if(cutAt > 0) {
            int type = 0;
            QByteArray endDelim = SEP;
            if(str.startsWith("Asterisk")) {
                type = 1;
                endDelim = SEPLN;
            } else if (str.startsWith("Response:")) {
                if(str.startsWith("Response: Follows")) {
                    type = 2;
                    endDelim = "\n--END COMMAND--\r\n\r\n";
                } else {
                    type = 3;
                }
            } else if(str.startsWith("Event:")) {
                type = 4;
            }

            if ((endAt = str.indexOf(endDelim)) == -1) {
                break;
            }

            QByteArray nstr = str.left(endAt);
            switch(type) {
            case 1:
                emit newVersion(nstr);
                break;
            case 2:
            case 3: {
                emit newResponse(nstr);
            }
                break;
            case 4: {
                emit newEvent(nstr);
                // debug
                if(nstr.isEmpty()) {
                    syslog(LOG_WARNING, "%s: EMPTY EVENT.\n"
                           "cutAt: %d\n"
                           "endAt: %d\n"
                           "strNew(%d): '%s'\n"
                           "strNew(HEX): %s\n"
                           "mStrUnprocessed(%d): '%s'\n"
                           "endDelim(HEX): %s",
                           Q_FUNC_INFO,
                           cutAt,
                           endAt,
                           strNew.size(),
                           strNew.constData(),
                           strNew.toHex().constData(),
                           m_Buffer.size(),
                           m_Buffer.constData(),
                           endDelim.toHex().constData());
                    qDebug("%s: EMPTY EVENT.\n"
                           "cutAt: %d\n"
                           "endAt: %d\n"
                           "strNew(%d): '%s'\n"
                           "strNew(HEX): %s\n"
                           "mStrUnprocessed(%d): '%s'\n"
                           "endDelim(HEX): %s",
                           Q_FUNC_INFO,
                           cutAt,
                           endAt,
                           strNew.size(),
                           strNew.constData(),
                           strNew.toHex().constData(),
                           m_Buffer.size(),
                           m_Buffer.constData(),
                           endDelim.toHex().constData()
                           );
                        qWarning("FAKE WARN");
                }
            }
                break;
            default: {
                // LOG
                syslog(LOG_WARNING, "INVALID Type Received: %s", nstr.constData());
                syslog(LOG_WARNING, "%s: EMPTY EVENT.\n"
                       "cutAt: %d\n"
                       "endAt: %d\n"
                       "strNew(%d): '%s'\n"
                       "strNew(HEX): %s\n"
                       "mStrUnprocessed(%d): '%s'\n"
                       "endDelim(HEX): %s",
                       Q_FUNC_INFO,
                       cutAt,
                       endAt,
                       strNew.size(),
                       strNew.constData(),
                       strNew.toHex().constData(),
                       m_Buffer.size(),
                       m_Buffer.constData(),
                       endDelim.toHex().constData());
                qDebug("INVALID Type Received: %s", nstr.constData());
                qDebug("%s: EMPTY EVENT.\n"
                       "cutAt: %d\n"
                       "endAt: %d\n"
                       "strNew(%d): '%s'\n"
                       "strNew(HEX): %s\n"
                       "mStrUnprocessed(%d): '%s'\n"
                       "endDelim(HEX): %s",
                       Q_FUNC_INFO,
                       cutAt,
                       endAt,
                       strNew.size(),
                       strNew.constData(),
                       strNew.toHex().constData(),
                       m_Buffer.size(),
                       m_Buffer.constData(),
                       endDelim.toHex().constData()
                       );
                    qWarning("FAKE WARN");
            }
                break;
            }
            str = str.mid(endAt + endDelim.length());
        } else
            str = str.left(SEPLN.length());
        cutAt++;
    }
    if(str.length() > 0) {
        m_Buffer.append(str); //saves data not processed
    }
}

void QAmiReader::socketRead()
{
    QByteArray buffer(m_Socket.readAll());
    emit debugReceived(buffer);
    processIncoming(buffer);
}

void QAmiReader::sendAction(QString str)
{
    m_Socket.write(str.toUtf8());
}
