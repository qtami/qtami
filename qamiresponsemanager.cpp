/*
 * Copyright © 2011-2012 CarelessChaser <chaser.andrey@gmail.com>
 *
 * This file is part of QtAmi
 *
 * QtAmi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QtAmi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QtAmi. If not, see <http://www.gnu.org/licenses/>.
 */
#include <QtCore/QtDebug>
#include <sys/syslog.h>


#include "qamiresponsemanager.h"
#include "qamiresponse.h"
#include "qamiutils.h"

using namespace QAmi;

QAmiResponseManager::QAmiResponseManager(QObject *parent) :
    QThread(parent)
{
}

QAmiResponseManager::~QAmiResponseManager()
{
}

void QAmiResponseManager::onResponse(QByteArray str)
{
    QAmiResponse response(str);
    QString id = response.actionId();

    if(id.isEmpty()) {
        const QString strLog = QAmi::str2log(str);
        syslog( LOG_WARNING, "Ignoring response without ActionID: \"%s\"", qPrintable(strLog) );
        qWarning("Ignoring response without ActionID: \"%s\"", qPrintable(strLog) );
        return;
    }


    //int elapsed = m_timers.take(id).elapsed();
    //FIXME: add timeout

    QTimer *timerTimeout = m_actionIdTimeout.take(id);
    m_timeoutActionId.remove(timerTimeout);
    delete timerTimeout;

    if(mActions.contains(id)) {
        QAmiAction action = mActions.take(id); //deleting action from list
        emit amiResponse(response, action);
        switch(action.type()) {
        case AcCoreShowChannels:
            emit responseCoreShowChannels(response);
            break;
        case AcDongleSendSMS:
            emit responseDongleSendSMS(response, action);
            break;
        case AcDongleSendUSSD:
            emit responseDongleSendUSSD(response, action);
            break;
        case AcDongleShowDevices:
            emit responseDongleShowDevices(response);
            break;
        case AcGetvar:
            emit responseGetvar(response, action);
            break;
        case AcListCommands:
            emit responseListCommands(response);
            break;
        case AcLogin:
            emit responseLogin(response);
            break;
        case AcOriginate:
            emit responseOriginate(response, action);
            break;
        case AcPing:
            emit responsePing(response, mTimerPingTime.elapsed());
            break;
        case AcQueuePause:
            emit responseQueuePause(response, action);
            break;
        case AcQueueStatus:
            emit responseQueueStatus(response);
            break;
        case AcSetvar:
            emit responseSetvar(response, action);
            break;
        case AcSIPshowpeer:
            emit responseSIPshowpeer(response);
            break;
        case AcStatus:
            emit responseStatus(response);
            break;
        default:
            syslog(LOG_WARNING, "%s: unknown response: '%s'", Q_FUNC_INFO, qPrintable(action.name()));
            syslog(LOG_WARNING, "RESPONSE: %s", qPrintable(str2log(response.toString())));
            qWarning("%s: unknown response: '%s'", Q_FUNC_INFO, qPrintable(action.name()));
            qWarning("RESPONSE: %s", qPrintable(str2log(response.toString())) );
            break;
        }
    } else {
        syslog(LOG_ERR, "%s:%d action not found with ActionID '%s'. Response: %s",
               Q_FUNC_INFO, __LINE__, qPrintable(id), qPrintable(str2log(response.toString())));
        qWarning("%s:%d action not found with ActionID '%s'. Response: %s",
                 Q_FUNC_INFO, __LINE__, qPrintable(id), qPrintable(str2log(response.toString())));
    }
}

void QAmiResponseManager::start()
{
    QThread::start();
}

void QAmiResponseManager::stop()
{
    QThread::quit();
}

void QAmiResponseManager::addAction(const QAmiAction &action, quint32 timeout_msec)
{
    const QString id = action.actionId();
    mActions[id] = action;
    if(timeout_msec > 0) {
        QTimer *timerTimeout_ptr = new QTimer(this);
        timerTimeout_ptr->setInterval(timeout_msec);
        connect(timerTimeout_ptr, SIGNAL(timeout()), this, SLOT(onTimeout()));

        m_actionIdTimeout.insert(id, timerTimeout_ptr);
        m_timeoutActionId.insert(timerTimeout_ptr, id);

        m_timers[id].start();
    }
}

void QAmiResponseManager::onTimeout()
{
    QTimer *timer = qobject_cast<QTimer*>(sender());
    if (timer == 0) {
        qWarning("%s: null pointer", Q_FUNC_INFO);
        return;
    }
    QString id = m_timeoutActionId.take(timer);
    m_actionIdTimeout.remove(id);
    timer->deleteLater();
    m_timers.remove(id);
}
