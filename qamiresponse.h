/*
 * Copyright © 2011-2012 CarelessChaser <chaser.andrey@gmail.com>
 *
 * This file is part of QtAmi
 *
 * QtAmi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QtAmi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QtAmi. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef QAMIRESPONSE_H
#define QAMIRESPONSE_H

#include "qamipropertymap.h"
#include "qamiaction.h"

namespace QAmi {

class QAmiResponse : public QAmiPropertyMap
{
public:
    QAmiResponse(const QAmiResponse &response);
    QAmiResponse(const QByteArray &str);
    QAmiResponse() : QAmiPropertyMap() { }
    virtual ~QAmiResponse();
    QString actionId() const;
    bool isSuccess() const;
};

class ResponseDongleSendSMS : public QAmiResponse
{
public:
    ResponseDongleSendSMS() { } //TODO: remove after migrate to Qt5
    ResponseDongleSendSMS(const QAmiResponse &response) : QAmiResponse(response) { }
    inline QString id() const { return value(KeyID); }
    inline QString message() const { return value(KeyMessage); }
};

class ResponseDongleSendUSSD : public QAmiResponse
{
public:
    ResponseDongleSendUSSD() {} //TODO: remove after migrate to Qt5
    ResponseDongleSendUSSD(const QAmiResponse &response) : QAmiResponse(response) { }
    inline QString message() const { return value(KeyMessage); }
};

class ResponseGetvar : public QAmiResponse
{
public:
    ResponseGetvar() { } //TODO: remove after migrate to Qt5
    ResponseGetvar(const QAmiResponse &response) : QAmiResponse(response) { }
    inline QString value() const { return QAmiResponse::value(KeyValue); }
};

class ResponseOriginate : public QAmiResponse
{
public:
    ResponseOriginate() { } //TODO: remove after migrate to Qt5
    ResponseOriginate(const QAmiResponse &response) : QAmiResponse(response) { }
    inline QString message() const { return value(KeyMessage); }
};

class ResponseQueuePause : public QAmiResponse
{
public:
    ResponseQueuePause() { } //TODO: remove after migrate to Qt5
    ResponseQueuePause(const QAmiResponse &response) : QAmiResponse(response) { }
    inline QString message() const { return value(KeyMessage); }
};

class ResponseSetvar : public QAmiResponse
{
public:
    ResponseSetvar() { } //TODO: remove after migrate to Qt5
    ResponseSetvar(const QAmiResponse &response) : QAmiResponse(response) { }
    inline QString message() const { return value(KeyMessage); }
};

}

#endif // QAMIRESPONSE_H
