/*
 * Copyright © 2011-2012 CarelessChaser <chaser.andrey@gmail.com>
 *
 * This file is part of QtAmi
 *
 * QtAmi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QtAmi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QtAmi. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef QAMIEVENT_H
#define QAMIEVENT_H

#include "qamipropertymap.h"
#include "qamistructs.h"

namespace QAmi {

class QAmiEvent : public QAmiPropertyMap
{
public:
    QAmiEvent(const QByteArray &str);
    QAmiEvent(const QAmiEvent &ev);

    virtual ~QAmiEvent();

    QString actionId() const;
    QString typeStr() const;
    EventType type() const;
private:
    EventType m_Type;
};

class EventAGIExec : public QAmiEvent
{
public:
    EventAGIExec(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString channel() const { return value(KeyChannel); }
    inline QString command() const { return value(KeyCommand); }
    inline QString commandId() const { return value(KeyCommandId); }
    inline QString privilege() const { return value(KeyPrivilege); }
    inline QString result() const { return value(KeyResult); }
    inline QString resultCode() const { return value(KeyResultCode); }
    inline QString subEvent() const { return value(KeySubEvent); }
};

class EventAgentCalled : public QAmiEvent
{
public:
    EventAgentCalled(const QAmiEvent &ev) : QAmiEvent(ev) { }
    //TODO: EventAgentCalled
};

class EventAgentComplete : public QAmiEvent
{
public:
    EventAgentComplete(const QAmiEvent &ev) : QAmiEvent(ev) { }
    //TODO: EventAgentComplete
};

class EventAgentConnect : public QAmiEvent
{
    EventAgentConnect(const QAmiEvent &ev) : QAmiEvent(ev) { }
    //TODO: EventAgentConnect
};

class EventAgentRingNoAnswer : public QAmiEvent
{
    EventAgentRingNoAnswer(const QAmiEvent &ev) : QAmiEvent(ev) { }
    //TODO: EventAgentRingNoAnswer
};

class EventBridge : public QAmiEvent
{
public:
    EventBridge(const QAmiEvent &ev) : QAmiEvent(ev) {  }
    inline QString bridgestate() const { return value(KeyBridgestate); }
    inline QString bridgetype() const { return value(KeyBridgetype); }
    inline QString callerID1() const { return value(KeyCallerID1); }
    inline QString callerID2() const { return value(KeyCallerID2); }
    inline QString channel1() const { return value(KeyChannel1); }
    inline QString channel2() const { return value(KeyChannel2); }
    inline QString privilege() const { return value(KeyPrivilege); }
    inline QString uniqueid1() const { return value(KeyUniqueid1); }
    inline QString uniqueid2() const { return value(KeyUniqueid2); }
};

class EventChannelReload : public QAmiEvent
{
public:
    EventChannelReload(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString channelType() const { return value(KeyChannelType); }
    inline QString peer_Count() const { return value(KeyPeer_Count); }
    inline QString privilege() const { return value(KeyPrivilege); }
    inline QString registry_Count() const { return value(KeyRegistry_Count); }
    inline QString reloadReason() const { return value(KeyReloadReason); }
};

class EventCoreShowChannel : public QAmiEvent
{
public:
    EventCoreShowChannel(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString accountCode() const { return value(KeyAccountCode); }
    inline QString actionID() const { return value(KeyActionID); }
    inline QString application() const { return value(KeyApplication); }
    inline QString applicationData() const { return value(KeyApplicationData); }
    inline QString bridgedChannel() const { return value(KeyBridgedChannel); }
    inline QString bridgedUniqueID() const { return value(KeyBridgedUniqueID); }
    inline QString callerIDname() const { return value(KeyCallerIDname); }
    inline QString callerIDnum() const { return value(KeyCallerIDnum); }
    inline QString channel() const { return value(KeyChannel); }
    inline QString channelState() const { return value(KeyChannelState); }
    inline QString channelStateDesc() const { return value(KeyChannelStateDesc); }
    inline QString connectedLineName() const { return value(KeyConnectedLineName); }
    inline QString connectedLineNum() const { return value(KeyConnectedLineNum); }
    inline QString context() const { return value(KeyContext); }
    inline QString duration() const { return value(KeyDuration); }
    inline QString extension() const { return value(KeyExtension); }
    inline QString uniqueID() const { return value(KeyUniqueID); }
};

class EventCoreShowChannelsComplete : public QAmiEvent
{
public:
    EventCoreShowChannelsComplete(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString actionID() const { return value(KeyActionID); }
    inline QString eventList() const { return value(KeyEventList); }
    inline QString listItems() const { return value(KeyListItems); }
};

class EventDial : public QAmiEvent
{
public:
    EventDial(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString callerIDName() const { return value(KeyCallerIDName); }
    inline QString callerIDNum() const { return value(KeyCallerIDNum); }
    inline QString channel() const { return value(KeyChannel); }
    inline QString connectedLineName() const { return value(KeyConnectedLineName); }
    inline QString connectedLineNum() const { return value(KeyConnectedLineNum); }
    inline QString destination() const { return value(KeyDestination); }
    inline QString destUniqueID() const { return value(KeyDestUniqueID); }
    inline QString dialstring() const { return value(KeyDialstring); }
    inline QString privilege() const { return value(KeyPrivilege); }
    inline QString subEvent() const { return value(KeySubEvent); }
    inline QString uniqueID() const { return value(KeyUniqueID); }
};

class EventDongleCallStateChange : public QAmiEvent
{
public:
    EventDongleCallStateChange(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString callIdx() const { return value(KeyCallIdx); }
    inline QString device() const { return value(KeyDevice); }
    inline QString newState() const { return value(KeyNewState); }
    inline QString privilege() const { return value(KeyPrivilege); }
};

class EventDongleCEND : public QAmiEvent
{
public:
    EventDongleCEND(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString callIdx() const { return value(KeyCallIdx); }
    inline QString CCCause() const { return value(KeyCCCause); }
    inline QString device() const { return value(KeyDevice); }
    inline QString duration() const { return value(KeyDuration); }
    inline QString endStatus() const { return value(KeyEndStatus); }
    inline QString privilege() const { return value(KeyPrivilege); }
};

class EventDongleDeviceEntry : public QAmiEvent
{
public:
    EventDongleDeviceEntry(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString actionID() const { return value(KeyActionID); }
    inline QString active() const { return value(KeyActive); }
    inline QString alerting() const { return value(KeyAlerting); }
    inline QString audioSetting() const { return value(KeyAudioSetting); }
    inline QString audioState() const { return value(KeyAudioState); }
    inline QString autoDeleteSMS() const { return value(KeyAutoDeleteSMS); }
    inline QString callsChannels() const { return value(KeyCallsChannels); }
    inline QString callWaitingSetting() const { return value(KeyCallWaitingSetting); }
    inline QString callWaitingState() const { return value(KeyCallWaitingState); }
    inline QString channelLanguage() const { return value(KeyChannelLanguage); }
    inline QString cellID() const { return value(KeyCellID); }
    inline QString commandsInQueue() const { return value(KeyCommandsInQueue); }
    inline QString context() const { return value(KeyContext); }
    inline QString currentDeviceState() const { return value(KeyCurrentDeviceState); }
    inline QString dataSetting() const { return value(KeyDataSetting); }
    inline QString dataState() const { return value(KeyDataState); }
    inline QString defaultCallingPres() const { return value(KeyDefaultCallingPres); }
    inline QString desiredDeviceState() const { return value(KeyDesiredDeviceState); }
    inline QString device() const { return value(KeyDevice); }
    inline QString dialing() const { return value(KeyDialing); }
    inline QString disableSMS() const { return value(KeyDisableSMS); }
    inline QString DTMF() const { return value(KeyDTMF); }
    inline QString exten() const { return value(KeyExten); }
    inline QString firmware() const { return value(KeyFirmware); }
    inline QString group() const { return value(KeyGroup); }
    inline QString GSMRegistrationStatus() const { return value(KeyGSMRegistrationStatus); }
    inline QString held() const { return value(KeyHeld); }
    inline QString IMEISetting() const { return value(KeyIMEISetting); }
    inline QString IMEIState() const { return value(KeyIMEIState); }
    inline QString IMSISetting() const { return value(KeyIMSISetting); }
    inline QString IMSIState() const { return value(KeyIMSIState); }
    inline QString incoming() const { return value(KeyIncoming); }
    inline QString initializing() const { return value(KeyInitializing); }
    inline QString locationAreaCode() const { return value(KeyLocationAreaCode); }
    inline QString manufacturer() const { return value(KeyManufacturer); }
    inline QString minimalDTMFDuration() const { return value(KeyMinimalDTMFDuration); }
    inline QString minimalDTMFGap() const { return value(KeyMinimalDTMFGap); }
    inline QString minimalDTMFInterval() const { return value(KeyMinimalDTMFInterval); }
    inline QString mode() const { return value(KeyMode); }
    inline QString model() const { return value(KeyModel); }
    inline QString providerName() const { return value(KeyProviderName); }
    inline QString releasing() const { return value(KeyReleasing); }
    inline QString resetDongle() const { return value(KeyResetDongle); }
    inline QString RSSI() const { return value(KeyRSSI); }
    inline QString RXGain() const { return value(KeyRXGain); }
    inline QString sms() const { return value(KeySMS); }
    inline QString SMSPDU() const { return value(KeySMSPDU); }
    inline QString SMSServiceCenter() const { return value(KeySMSServiceCenter); }
    inline QString state() const { return value(KeyState); }
    inline QString submode() const { return value(KeySubmode); }
    inline QString subscriberNumber() const { return value(KeySubscriberNumber); }
    inline QString tasksInQueue() const { return value(KeyTasksInQueue); }
    inline QString TXGain() const { return value(KeyTXGain); }
    inline QString U2DIAG() const { return value(KeyU2DIAG); }
    inline QString useCallingPres() const { return value(KeyUseCallingPres); }
    inline QString useUCS2Encoding() const { return value(KeyUseUCS2Encoding); }
    inline QString USSDUse7BitEncoding() const { return value(KeyUSSDUse7BitEncoding); }
    inline QString USSDUseUCS2Decoding() const { return value(KeyUSSDUseUCS2Decoding); }
    inline QString voice() const { return value(KeyVoice); }
    inline QString waiting() const { return value(KeyWaiting); }
};

class EventDongleNewCMGR : public QAmiEvent
{
    EventDongleNewCMGR(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString device() const { return value(KeyDevice); }
    inline QString message() const { return value(KeyMessage); }
    inline QString privilege() const { return value(KeyPrivilege); }
};

class EventDongleNewCUSD : public QAmiEvent
{
    EventDongleNewCUSD(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString device() const { return value(KeyDevice); }
    inline QString message() const { return value(KeyMessage); }
    inline QString privilege() const { return value(KeyPrivilege); }
};

class EventDongleNewSMSBase64 : public QAmiEvent
{
    EventDongleNewSMSBase64(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString device() const { return value(KeyDevice); }
    inline QString from() const { return value(KeyFrom); }
    inline QString message() const { return value(KeyMessage); }
    inline QString privilege() const { return value(KeyPrivilege); }
};

class EventDongleNewUSSDBase64 : public QAmiEvent
{
public:
    EventDongleNewUSSDBase64(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString device() const { return value(KeyDevice); }
    inline QString message() const { return value(KeyMessage); }
    inline QString privilege() const { return value(KeyPrivilege); }
};

class EventDonglePortFail : public QAmiEvent
{
public:
    EventDonglePortFail(const QAmiEvent &ev) : QAmiEvent(ev) { }
    //TODO: EventDonglePortFail
};

class EventDongleShowDevicesComplete : public QAmiEvent
{
public:
    EventDongleShowDevicesComplete(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString actionID() const { return value(KeyActionID); }
    inline QString eventList() const { return value(KeyEventList); }
    inline QString listItems() const { return value(KeyListItems); }
};

class EventDongleSMSStatus : public QAmiEvent
{
public:
    EventDongleSMSStatus(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString device() const { return value(KeyDevice); }
    inline QString privilege() const { return value(KeyPrivilege); }
    inline QString ID() const { return value(KeyID); }
    inline QString status() const { return value(KeyStatus); }
};

class EventDongleStatus : public QAmiEvent
{
public:
    EventDongleStatus(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString privilege() const { return value(KeyPrivilege); }
    inline QString device() const { return value(KeyDevice); }
    inline QString status() const { return value(KeyStatus); }
};

class EventDongleUSSDStatus : public QAmiEvent
{
    EventDongleUSSDStatus(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString device() const { return value(KeyDevice); }
    inline QString privilege() const { return value(KeyPrivilege); }
    inline QString ID() const { return value(KeyID); }
    inline QString status() const { return value(KeyStatus); }
};

class EventDTMF : public QAmiEvent
{
public:
    EventDTMF(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString begin() const { return value(KeyBegin); }
    inline QString channel() const { return value(KeyChannel); }
    inline QString digit() const { return value(KeyDigit); }
    inline QString direction() const { return value(KeyDirection); }
    inline QString end() const { return value(KeyEnd); }
    inline QString privilege() const { return value(KeyPrivilege); }
    inline QString uniqueid() const { return value(KeyUniqueid); }
};

class EventFullyBooted : public QAmiEvent
{
public:
    EventFullyBooted(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString privilege() const { return value(KeyPrivilege); }
    inline QString status() const { return value(KeyStatus); }
};

class EventHangup : public QAmiEvent
{
public:
    EventHangup(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString callerIDName() const { return value(KeyCallerIDName); }
    inline QString callerIDNum() const { return value(KeyCallerIDNum); }
    inline QString cause() const { return value(KeyCause); }
    inline QString causeTxt() const { return value(KeyCauseTxt); }
    inline QString channel() const { return value(KeyChannel); }
    inline QString connectedLineName() const { return value(KeyConnectedLineName); }
    inline QString connectedLineNum() const { return value(KeyConnectedLineNum); }
    inline QString privilege() const { return value(KeyPrivilege); }
    inline QString uniqueid() const { return value(KeyUniqueid); }
};

class EventJoin : public QAmiEvent
{
public:
    EventJoin(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString callerIDName() const { return value(KeyCallerIDName); }
    inline QString callerIDNum() const { return value(KeyCallerIDNum); }
    inline QString channel() const { return value(KeyChannel); }
    inline QString connectedLineName() const { return value(KeyConnectedLineName); }
    inline QString connectedLineNum() const { return value(KeyConnectedLineNum); }
    inline QString count() const { return value(KeyCount); }
    inline QString position() const { return value(KeyPosition); }
    inline QString privilege() const { return value(KeyPrivilege); }
    inline QString queue() const { return value(KeyQueue); }
    inline QString uniqueid() const { return value(KeyUniqueid); }
};

class EventLeave : public QAmiEvent
{
public:
    EventLeave(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString channel() const { return value(KeyChannel); }
    inline QString count() const { return value(KeyCount); }
    inline QString position() const { return value(KeyPosition); }
    inline QString privilege() const { return value(KeyPrivilege); }
    inline QString queue() const { return value(KeyQueue); }
    inline QString uniqueid() const { return value(KeyUniqueid); }
};

class EventMasquerade : public QAmiEvent
{
public:
    EventMasquerade(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString privilege() const { return value(KeyPrivilege); }
    inline QString clone() const { return value(KeyClone); }
    inline QString cloneState() const { return value(KeyCloneState); }
    inline QString original() const { return value(KeyOriginal); }
    inline QString originalState() const { return value(KeyOriginalState); }
};

class EventMusicOnHold : public QAmiEvent
{
public:
    EventMusicOnHold(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString channel() const { return value(KeyChannel); }
    inline QString privilege() const { return value(KeyPrivilege); }
    inline QString state() const { return value(KeyState); }
    inline QString uniqueID() const { return value(KeyUniqueID); }
};

class EventNewAccountCode : public QAmiEvent
{
public:
    EventNewAccountCode(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString accountCode() const { return value(KeyAccountCode); }
    inline QString channel() const { return value(KeyChannel); }
    inline QString oldAccountCode() const { return value(KeyOldAccountCode); }
    inline QString privilege() const { return value(KeyPrivilege); }
    inline QString uniqueid() const { return value(KeyUniqueid); }
};

class EventNewCallerid : public QAmiEvent
{
public:
    EventNewCallerid(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString callerIDName() const { return value(KeyCallerIDName); }
    inline QString callerIDNum() const { return value(KeyCallerIDNum); }
    inline QString channel() const { return value(KeyChannel); }
    inline QString CID_CallingPres() const { return value(KeyCID_CallingPres); }
    inline QString privilege() const { return value(KeyPrivilege); }
    inline QString uniqueid() const { return value(KeyUniqueid); }
};

class EventNewchannel : public QAmiEvent
{
public:
    EventNewchannel(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString accountCode() const { return value(KeyAccountCode); }
    inline QString callerIDName() const { return value(KeyCallerIDName); }
    inline QString callerIDNum() const { return value(KeyCallerIDNum); }
    inline QString channel() const { return value(KeyChannel); }
    inline QString channelState() const { return value(KeyChannelState); }
    inline QString channelStateDesc() const { return value(KeyChannelStateDesc); }
    inline QString context() const { return value(KeyContext); }
    inline QString exten() const { return value(KeyExten); }
    inline QString privilege() const { return value(KeyPrivilege); }
    inline QString uniqueid() const { return value(KeyUniqueid); }
};

class EventNewexten : public QAmiEvent
{
public:
    EventNewexten(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString appData() const { return value(KeyAppData); }
    inline QString application() const { return value(KeyApplication); }
    inline QString channel() const { return value(KeyChannel); }
    inline QString context() const { return value(KeyContext); }
    inline QString extension() const { return value(KeyExtension); }
    inline QString priority() const { return value(KeyPriority); }
    inline QString privilege() const { return value(KeyPrivilege); }
    inline QString uniqueid() const { return value(KeyUniqueid); }
};

class EventNewstate : public QAmiEvent
{
public:
    EventNewstate(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString callerIDName() const { return value(KeyCallerIDName); }
    inline QString callerIDNum() const { return value(KeyCallerIDNum); }
    inline QString channel() const { return value(KeyChannel); }
    inline QString channelState() const { return value(KeyChannelState); }
    inline QString channelStateDesc() const { return value(KeyChannelStateDesc); }
    inline QString connectedLineName() const { return value(KeyConnectedLineName); }
    inline QString connectedLineNum() const { return value(KeyConnectedLineNum); }
    inline QString privilege() const { return value(KeyPrivilege); }
    inline QString uniqueid() const { return value(KeyUniqueid); }
};

class EventPeerEntry : public QAmiEvent
{
public:
    EventPeerEntry(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString ACL() const { return value(KeyACL); }
    inline QString actionID() const { return value(KeyActionID); }
    inline QString channeltype() const { return value(KeyChannelType); }
    inline QString chanObjectType() const { return value(KeyChanObjectType); }
    inline QString dynamic() const { return value(KeyDynamic); }
    inline QString forcerport() const { return value(KeyForcerport); }
    inline QString IPaddress() const { return value(KeyIPaddress); }
    inline QString IPport() const { return value(KeyIPport); }
    inline QString objectName() const { return value(KeyObjectName); }
    inline QString realtimeDevice() const { return value(KeyRealtimeDevice); }
    inline QString status() const { return value(KeyStatus); }
    inline QString videoSupport() const { return value(KeyVideoSupport); }
};

class EventPeerStatus : public QAmiEvent
{
public:
    EventPeerStatus(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString address() const { return value(KeyAddress); }
    inline QString channelType() const { return value(KeyChannelType); }
    inline QString peer() const { return value(KeyPeer); }
    inline QString peerStatus() const { return value(KeyPeerStatus); }
    inline QString privilege() const { return value(KeyPrivilege); }
};

class EventQueueCallerAbandon : public QAmiEvent
{
public:
    EventQueueCallerAbandon(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString holdTime() const { return value(KeyHoldTime); }
    inline QString originalPosition() const { return value(KeyOriginalPosition); }
    inline QString position() const { return value(KeyPosition); }
    inline QString privilege() const { return value(KeyPrivilege); }
    inline QString queue() const { return value(KeyQueue); }
    inline QString uniqueid() const { return value(KeyUniqueid); }
};

class EventQueueEntry : public QAmiEvent
{
public:
    EventQueueEntry(const QAmiEvent &ev) : QAmiEvent(ev) { }
    //TODO: EventQueueEntry
};

class EventQueueMember : public QAmiEvent
{
public:
    EventQueueMember(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString actionID() const { return value(KeyActionID); }
    inline QString callsTaken() const { return value(KeyCallsTaken); }
    inline QString lastCall() const { return value(KeyLastCall); }
    inline QString location() const { return value(KeyLocation); }
    inline QString membership() const { return value(KeyMembership); }
    inline QString name() const { return value(KeyName); }
    inline QString paused() const { return value(KeyPaused); }
    inline QString penalty() const { return value(KeyPenalty); }
    inline QString queue() const { return value(KeyQueue); }
    inline QString status() const { return value(KeyStatus); }
};

class EventQueueMemberAdded : public QAmiEvent
{
public:
    EventQueueMemberAdded(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString callsTaken() const { return value(KeyCallsTaken); }
    inline QString lastCall() const { return value(KeyLastCall); }
    inline QString location() const { return value(KeyLocation); }
    inline QString memberName() const { return value(KeyMemberName); }
    inline QString membership() const { return value(KeyMembership); }
    inline QString status() const { return value(KeyStatus); }
    inline QString paused() const { return value(KeyPaused); }
    inline QString penalty() const { return value(KeyPenalty); }
    inline QString privilege() const { return value(KeyPrivilege); }
    inline QString queue() const { return value(KeyQueue); }
};

class EventQueueMemberPaused : public QAmiEvent
{
public:
    EventQueueMemberPaused(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString location() const { return value(KeyLocation); }
    inline QString memberName() const { return value(KeyMemberName); }
    inline QString paused() const { return value(KeyPaused); }
    inline QString privilege() const { return value(KeyPrivilege); }
    inline QString queue() const { return value(KeyQueue); }
    inline QString reason() const { return value(KeyReason); }
};

class EventQueueMemberRemoved : public QAmiEvent
{
public:
    EventQueueMemberRemoved(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString location() const { return value(KeyLocation); }
    inline QString memberName() const { return value(KeyMemberName); }
    inline QString privilege() const { return value(KeyPrivilege); }
    inline QString queue() const { return value(KeyQueue); }
};

class EventQueueMemberStatus : public QAmiEvent
{
public:
    EventQueueMemberStatus(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString callsTaken() const { return value(KeyCallsTaken); }
    inline QString lastCall() const { return value(KeyLastCall); }
    inline QString location() const { return value(KeyLocation); }
    inline QString memberName() const { return value(KeyMemberName); }
    inline QString membership() const { return value(KeyMembership); }
    inline QString paused() const { return value(KeyPaused); }
    inline QString penalty() const { return value(KeyPenalty); }
    inline QString privilege() const { return value(KeyPrivilege); }
    inline QString queue() const { return value(KeyQueue); }
    inline QString status() const { return value(KeyStatus); }
};

class EventQueueParams : public QAmiEvent
{
public:
    EventQueueParams(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString abandoned() const { return value(KeyAbandoned); }
    inline QString actionID() const { return value(KeyActionID); }
    inline QString calls() const { return value(KeyCalls); }
    inline QString completed() const { return value(KeyCompleted); }
    inline QString holdtime() const { return value(KeyHoldtime); }
    inline QString max() const { return value(KeyMax); }
    inline QString queue() const { return value(KeyQueue); }
    inline QString serviceLevel() const { return value(KeyServiceLevel); }
    inline QString servicelevelPerf() const { return value(KeyServicelevelPerf); }
    inline QString strategy() const { return value(KeyStrategy); }
    inline QString talkTime() const { return value(KeyTalkTime); }
};

class EventQueueStatusComplete : public QAmiEvent
{
public:
    EventQueueStatusComplete(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString actionID() const { return value(KeyActionID); }
};

class EventRegistry : public QAmiEvent
{
public:
    EventRegistry(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString channelType() const { return value(KeyChannelType); }
    inline QString domain() const { return value(KeyDomain); }
    inline QString privilege() const { return value(KeyPrivilege); }
    inline QString status() const { return value(KeyStatus); }
};

class EventRename : public QAmiEvent
{
public:
    EventRename(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString channel() const { return value(KeyChannel); }
    inline QString newname() const { return value(KeyNewname); }
    inline QString privilege() const { return value(KeyPrivilege); }
    inline QString uniqueid() const { return value(KeyUniqueid); }
};

class EventRTCPReceived : public QAmiEvent
{
public:
    EventRTCPReceived(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString DLSR() const { return value(KeyDLSR); }
    inline QString fractionLost() const { return value(KeyFractionLost); }
    inline QString from() const { return value(KeyFrom); }
    inline QString highestSequence() const { return value(KeyHighestSequence); }
    inline QString IAJitter() const { return value(KeyIAJitter); }
    inline QString lastSR() const { return value(KeyLastSR); }
    inline QString packetsLost() const { return value(KeyPacketsLost); }
    inline QString privilege() const { return value(KeyPrivilege); }
    inline QString PT() const { return value(KeyPT); }
    inline QString receptionReports() const { return value(KeyReceptionReports); }
    inline QString RTT() const { return value(KeyRTT); }
    inline QString senderSSRC() const { return value(KeySenderSSRC); }
    inline QString sequenceNumberCycles() const { return value(KeySequenceNumberCycles); }
};

class EventRTCPSent : public QAmiEvent
{
public:
    EventRTCPSent(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString cumulativeLoss() const { return value(KeyCumulativeLoss); }
    inline QString DLSR() const { return value(KeyDLSR); }
    inline QString fractionLost() const { return value(KeyFractionLost); }
    inline QString IAJitter() const { return value(KeyIAJitter); }
    inline QString ourSSRC() const { return value(KeyOurSSRC); }
    inline QString privilege() const { return value(KeyPrivilege); }
    inline QString reportBlock() const { return value(KeyReportBlock); }
    inline QString sentNTP() const { return value(KeySentNTP); }
    inline QString sentOctets() const { return value(KeySentOctets); }
    inline QString sentPackets() const { return value(KeySentPackets); }
    inline QString sentRTP() const { return value(KeySentRTP); }
    inline QString theirLastSR() const { return value(KeyTheirLastSR); }
    inline QString to() const { return value(KeyTo); }
};

class EventShutdown : public QAmiEvent
{
public:
    EventShutdown(const QAmiEvent &ev) : QAmiEvent(ev) { }
    //TODO: EventShutdown
};

class EventStatus : public QAmiEvent
{
public:
    EventStatus(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString account() const { return value(KeyAccount); }
    inline QString accountcode() const { return value(KeyAccountcode); }
    inline QString actionID() const { return value(KeyActionID); }
    inline QString callerIDName() const { return value(KeyCallerIDName); }
    inline QString callerIDNum() const { return value(KeyCallerIDNum); }
    inline QString channel() const { return value(KeyChannel); }
    inline QString channelState() const { return value(KeyChannelState); }
    inline QString channelStateDesc() const { return value(KeyChannelStateDesc); }
    inline QString connectedLineName() const { return value(KeyConnectedLineName); }
    inline QString connectedLineNum() const { return value(KeyConnectedLineNum); }
    inline QString context() const { return value(KeyContext); }
    inline QString extension() const { return value(KeyExtension); }
    inline QString priority() const { return value(KeyPriority); }
    inline QString privilege() const { return value(KeyPrivilege); }
    inline QString seconds() const { return value(KeySeconds); }
    inline QString state() const { return value(KeyState); }
    inline QString uniqueid() const { return value(KeyUniqueid); }
};

class EventStatusComplete : public QAmiEvent
{
public:
    EventStatusComplete(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString actionID() const { return value(KeyActionID); }
    inline QString items() const { return value(KeyItems); }
};

class EventTransfer : public QAmiEvent
{
public:
    EventTransfer(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString channel() const { return value(KeyChannel); }
    inline QString privilege() const { return value(KeyPrivilege); }
    inline QString SIP_Callid() const { return value(KeySIP_Callid); }
    inline QString targetChannel() const { return value(KeyTargetChannel); }
    inline QString targetUniqueid() const { return value(KeyTargetUniqueid); }
    inline QString transferContext() const { return value(KeyTransferContext); }
    inline QString transferExten() const { return value(KeyTransferExten); }
    inline QString transferMethod() const { return value(KeyTransferMethod); }
    inline QString transferType() const { return value(KeyTransferType); }
    inline QString uniqueid() const { return value(KeyUniqueid); }
};

class EventUnlink : public QAmiEvent
{
public:
    EventUnlink(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString privilege() const { return value(KeyPrivilege); }
    inline QString channel1() const { return value(KeyChannel1); }
    inline QString channel2() const { return value(KeyChannel2); }
    inline QString uniqueid1() const { return value(KeyUniqueid1); }
    inline QString uniqueid2() const { return value(KeyUniqueid2); }
    inline QString callerID1() const { return value(KeyCallerID1); }
    inline QString callerID2() const { return value(KeyCallerID2); }
};

class EventVarSet : public QAmiEvent
{
public:
    EventVarSet(const QAmiEvent &ev) : QAmiEvent(ev) { }
    inline QString privilege() const { return QAmiEvent::value(KeyPrivilege); }
    inline QString channel() const { return QAmiEvent::value(KeyChannel); }
    inline QString variable() const { return QAmiEvent::value(KeyVariable); }
    inline QString value() const { return QAmiEvent::value(KeyValue); }
    inline QString uniqueid() const { return QAmiEvent::value(KeyUniqueid); }
};

}

#endif // QAMIEVENT_H
