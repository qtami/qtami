/*
 * Copyright © 2011-2012 CarelessChaser <chaser.andrey@gmail.com>
 *
 * This file is part of QtAmi
 *
 * QtAmi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QtAmi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QtAmi. If not, see <http://www.gnu.org/licenses/>.
 */
#include "qamistructs.h"

#include <QtCore/QString>

namespace QAmi {

QHash<ActionType, QString> initActionTypes()
{
    QHash<ActionType, QString> m;
    m[AcCommand]            = "Command";
    m[AcCoreShowChannels]   = "CoreShowChannels";
    m[AcDongleReset]        = "DongleReset";
    m[AcDongleSendSMS]      = "DongleSendSMS";
    m[AcDongleSendUSSD]     = "DongleSendUSSD";
    m[AcDongleShowDevices]  = "DongleShowDevices";
    m[AcGetvar]             = "Getvar";
    m[AcHangup]             = "Hangup";
    m[AcListCommands]       = "ListCommands";
    m[AcLogin]              = "Login";
    m[AcOriginate]          = "Originate";
    m[AcPing]               = "Ping";
    m[AcSetvar]             = "Setvar";
    m[AcSippeers]           = "Sippeers";
    m[AcSIPshowpeer]        = "SIPshowpeer";
    m[AcStatus]             = "Status";
    m[AcQueueAdd]           = "QueueAdd";
    m[AcQueuePause]         = "QueuePause";
    m[AcQueueRemove]        = "QueueRemove";
    m[AcQueueStatus]        = "QueueStatus";
    m.squeeze();
    return m;
}

QMap<QString, ActionType> initActionStrTypes()
{
    QMap<QString, ActionType> m;
    QHash<ActionType, QString>::const_iterator i;
    for(i = ActionTypes.constBegin(); i != ActionTypes.constEnd(); ++i)
        m.insert(i.value(), i.key());
    return m;
}

QHash<EventType, QString> initEventTypes()
{
    QHash<EventType, QString> m;
    m[EvTypeAGIExec]                    = "AGIExec";
    m[EvTypeAgentCalled]                = "AgentCalled";
    m[EvTypeAgentComplete]              = "AgentComplete";
    m[EvTypeAgentConnect]               = "AgentConnect";
    m[EvTypeAgentRingNoAnswer]          = "AgentRingNoAnswer";
    m[EvTypeBridge]                     = "Bridge";
    m[EvTypeChannelReload]              = "ChannelReload";
    m[EvTypeChannelUpdate]              = "ChannelUpdate";
    m[EvTypeCoreShowChannel]            = "CoreShowChannel";
    m[EvTypeCoreShowChannelsComplete]   = "CoreShowChannelsComplete";
    m[EvTypeDial]                       = "Dial";
    m[EvTypeDongleCallStateChange]      = "DongleCallStateChange";
    m[EvTypeDongleCEND]                 = "DongleCEND";
    m[EvTypeDongleDeviceEntry]          = "DongleDeviceEntry";
    m[EvTypeDongleNewCMGR]              = "DongleNewCMGR";
    m[EvTypeDongleNewCUSD]              = "DongleNewCUSD";
    m[EvTypeDongleNewSMS]               = "DongleNewSMS";
    m[EvTypeDongleNewSMSBase64]         = "DongleNewSMSBase64";
    m[EvTypeDongleNewUSSD]              = "DongleNewUSSD";
    m[EvTypeDongleNewUSSDBase64]        = "DongleNewUSSDBase64";
    m[EvTypeDonglePortFail]             = "DonglePortFail";
    m[EvTypeDongleShowDevicesComplete]  = "DongleShowDevicesComplete";
    m[EvTypeDongleSMSStatus]            = "DongleSMSStatus";
    m[EvTypeDongleStatus]               = "DongleStatus";
    m[EvTypeDongleUSSDStatus]           = "DongleUSSDStatus";
    m[EvTypeDTMF]                       = "DTMF";
    m[EvTypeFullyBooted]                = "FullyBooted";
    m[EvTypeHangup]                     = "Hangup";
    m[EvTypeJoin]                       = "Join";
    m[EvTypeLeave]                      = "Leave";
    m[EvTypeMasquerade]                 = "Masquerade";
    m[EvTypeMusicOnHold]                = "MusicOnHold";
    m[EvTypeNewAccountCode]             = "NewAccountCode";
    m[EvTypeNewCallerid]                = "NewCallerid";
    m[EvTypeNewchannel]                 = "Newchannel";
    m[EvTypeNewexten]                   = "Newexten";
    m[EvTypeNewstate]                   = "Newstate";
    m[EvTypePeerEntry]                  = "PeerEntry";
    m[EvTypePeerStatus]                 = "PeerStatus";
    m[EvTypeQueueCallerAbandon]         = "QueueCallerAbandon";
    m[EvTypeQueueEntry]                 = "QueueEntry";
    m[EvTypeQueueMember]                = "QueueMember";
    m[EvTypeQueueMemberAdded]           = "QueueMemberAdded";
    m[EvTypeQueueMemberPaused]          = "QueueMemberPaused";
    m[EvTypeQueueMemberRemoved]         = "QueueMemberRemoved";
    m[EvTypeQueueMemberStatus]          = "QueueMemberStatus";
    m[EvTypeQueueParams]                = "QueueParams";
    m[EvTypeQueueStatusComplete]        = "QueueStatusComplete";
    m[EvTypeRegistry]                   = "Registry";
    m[EvTypeRename]                     = "Rename";
    m[EvTypeRTCPReceived]               = "RTCPReceived";
    m[EvTypeRTCPSent]                   = "RTCPSent";
    m[EvTypeShutdown]                   = "Shutdown";
    m[EvTypeStatus]                     = "Status";
    m[EvTypeStatusComplete]             = "StatusComplete";
    m[EvTypeTransfer]                   = "Transfer";
    m[EvTypeUnlink]                     = "Unlink";
    m[EvTypeVarSet]                     = "VarSet";
    m.squeeze();
    return m;
}

QMap<QString, EventType> initEventStrTypes()
{
    QMap<QString, EventType> m;
    QHash<EventType, QString>::const_iterator i;
    for(i = EventTypes.constBegin(); i != EventTypes.constEnd(); ++i)
        m.insert(i.value(), i.key());
    return m;
}

QHash<AmiKey, QString> initAmiKeys()
{
    QHash<AmiKey, QString> m;
    m[KeyAbandoned]             = "Abandoned";
    m[KeyAccount]               = "Account";
    m[KeyAccountCode]           = "AccountCode";
    m[KeyAccountcode]           = "Accountcode";
    m[KeyACL]                   = "ACL";
    m[KeyActionID]              = "ActionID";
    m[KeyActive]                = "Active";
    m[KeyAddress]               = "Address";
    m[KeyAgentName]             = "AgentName";
    m[KeyAlerting]              = "Alerting";
    m[KeyAppData]               = "AppData";
    m[KeyApplication]           = "Application";
    m[KeyApplicationData]       = "ApplicationData";
    m[KeyAsync]                 = "Async";
    m[KeyAudioSetting]          = "AudioSetting";
    m[KeyAudioState]            = "AudioState";
    m[KeyAutoDeleteSMS]         = "AutoDeleteSMS";
    m[KeyBegin]                 = "Begin";
    m[KeyBridgedChannel]        = "BridgedChannel";
    m[KeyBridgedUniqueID]       = "BridgedUniqueID";
    m[KeyBridgestate]           = "Bridgestate";
    m[KeyBridgetype]            = "Bridgetype";
    m[KeyCallerid]              = "Callerid";
    m[KeyCallerID1]             = "CallerID1";
    m[KeyCallerID2]             = "CallerID2";
    m[KeyCallerIDName]          = "CallerIDName";
    m[KeyCallerIDname]          = "CallerIDname";
    m[KeyCallerIDNum]           = "CallerIDNum";
    m[KeyCallerIDnum]           = "CallerIDnum";
    m[KeyCallIdx]               = "CallIdx";
    m[KeyCalls]                 = "Calls";
    m[KeyCallsChannels]         = "CallsChannels";
    m[KeyCallsTaken]            = "CallsTaken";
    m[KeyCallWaitingSetting]    = "CallWaitingSetting";
    m[KeyCallWaitingState]      = "CallWaitingState";
    m[KeyCause]                 = "Cause";
    m[KeyCauseTxt]              = "Cause-txt";
    m[KeyCCCause]               = "CCCause";
    m[KeyCellID]                = "CellID";
    m[KeyChannel]               = "Channel";
    m[KeyChannel1]              = "Channel1";
    m[KeyChannel2]              = "Channel2";
    m[KeyChannelLanguage]       = "ChannelLanguage";
    m[KeyChannelState]          = "ChannelState";
    m[KeyChannelStateDesc]      = "ChannelStateDesc";
    m[KeyChannelType]           = "ChannelType";
    m[KeyChanneltype]           = "Channeltype";
    m[KeyChanObjectType]        = "ChanObjectType";
    m[KeyCID_CallingPres]       = "CID-CallingPres";
    m[KeyClone]                 = "Clone";
    m[KeyCloneState]            = "CloneState";
    m[KeyCommand]               = "Command";
    m[KeyCommandId]             = "CommandId";
    m[KeyCommandsInQueue]       = "CommandsInQueue";
    m[KeyCompleted]             = "Completed";
    m[KeyConnectedLineName]     = "ConnectedLineName";
    m[KeyConnectedLineNum]      = "ConnectedLineNum";
    m[KeyContext]               = "Context";
    m[KeyCount]                 = "Count";
    m[KeyCumulativeLoss]        = "CumulativeLoss";
    m[KeyCurrentDeviceState]    = "CurrentDeviceState";
    m[KeyDataSetting]           = "DataSetting";
    m[KeyDataState]             = "DataState";
    m[KeyDefaultCallingPres]    = "DefaultCallingPres";
    m[KeyDesiredDeviceState]    = "DesiredDeviceState";
    m[KeyDestination]           = "Destination";
    m[KeyDestUniqueID]          = "DestUniqueID";
    m[KeyDevice]                = "Device";
    m[KeyDialing]               = "Dialing";
    m[KeyDialstring]            = "Dialstring";
    m[KeyDigit]                 = "Digit";
    m[KeyDirection]             = "Direction";
    m[KeyDisableSMS]            = "DisableSMS";
    m[KeyDLSR]                  = "DLSR";
    m[KeyDomain]                = "Domain";
    m[KeyDTMF]                  = "DTMF";
    m[KeyDynamic]               = "Dynamic";
    m[KeyDuration]              = "Duration";
    m[KeyEnd]                   = "End";
    m[KeyEndStatus]             = "EndStatus";
    m[KeyEvent]                 = "Event";
    m[KeyEventList]             = "EventList";
    m[KeyExten]                 = "Exten";
    m[KeyExtension]             = "Extension";
    m[KeyFirmware]              = "Firmware";
    m[KeyForcerport]            = "Forcerport";
    m[KeyFractionLost]          = "FractionLost";
    m[KeyFrom]                  = "From";
    m[KeyGroup]                 = "Group";
    m[KeyGSMRegistrationStatus] = "GSMRegistrationStatus";
    m[KeyHeld]                  = "Held";
    m[KeyHighestSequence]       = "HighestSequence";
    m[KeyHoldTime]              = "HoldTime";
    m[KeyHoldtime]              = "Holdtime";
    m[KeyIAJitter]              = "IAJitter";
    m[KeyID]                    = "ID";
    m[KeyIMEISetting]           = "IMEISetting";
    m[KeyIMEIState]             = "IMEIState";
    m[KeyIMSISetting]           = "IMSISetting";
    m[KeyIMSIState]             = "IMSIState";
    m[KeyIncoming]              = "Incoming";
    m[KeyInitializing]          = "Initializing";
    m[KeyInterface]             = "Interface";
    m[KeyIPaddress]             = "IPaddress";
    m[KeyIPport]                = "IPport";
    m[KeyItems]                 = "Items";
    m[KeyLastCall]              = "LastCall";
    m[KeyLastSR]                = "LastSR";
    m[KeyListItems]             = "ListItems";
    m[KeyLocation]              = "Location";
    m[KeyLocationAreaCode]      = "LocationAreaCode";
    m[KeyManufacturer]          = "Manufacturer";
    m[KeyMax]                   = "Max";
    m[KeyMemberName]            = "MemberName";
    m[KeyMembership]            = "Membership";
    m[KeyMessage]               = "Message";
    m[KeyMinimalDTMFDuration]   = "MinimalDTMFDuration";
    m[KeyMinimalDTMFGap]        = "MinimalDTMFGap";
    m[KeyMinimalDTMFInterval]   = "MinimalDTMFInterval";
    m[KeyMode]                  = "Mode";
    m[KeyModel]                 = "Model";
    m[KeyName]                  = "Name";
    m[KeyNewname]               = "Newname";
    m[KeyNewState]              = "NewState";
    m[KeyNumber]                = "Number";
    m[KeyObjectName]            = "ObjectName";
    m[KeyOldAccountCode]        = "OldAccountCode";
    m[KeyOriginal]              = "Original";
    m[KeyOriginalPosition]      = "OriginalPosition";
    m[KeyOriginalState]         = "OriginalState";
    m[KeyOurSSRC]               = "OurSSRC";
    m[KeyPacketsLost]           = "PacketsLost";
    m[KeyPaused]                = "Paused";
    m[KeyPeer]                  = "Peer";
    m[KeyPeer_Count]            = "Peer_Count";
    m[KeyPeerStatus]            = "PeerStatus";
    m[KeyPenalty]               = "Penalty";
    m[KeyPosition]              = "Position";
    m[KeyPriority]              = "Priority";
    m[KeyPrivilege]             = "Privilege";
    m[KeyProviderName]          = "ProviderName";
    m[KeyPT]                    = "PT";
    m[KeyQueue]                 = "Queue";
    m[KeyRealtimeDevice]        = "RealtimeDevice";
    m[KeyReason]                = "Reason";
    m[KeyReceptionReports]      = "ReceptionReports";
    m[KeyRegistry_Count]        = "Registry_Count";
    m[KeyReleasing]             = "Releasing";
    m[KeyReloadReason]          = "ReloadReason";
    m[KeyReportBlock]           = "ReportBlock";
    m[KeyResetDongle]           = "ResetDongle";
    m[KeyResult]                = "Result";
    m[KeyResultCode]            = "ResultCode";
    m[KeyRSSI]                  = "RSSI";
    m[KeyRTT]                   = "RTT";
    m[KeyRXGain]                = "RXGain";
    m[KeySeconds]               = "Seconds";
    m[KeySecret]                = "Secret";
    m[KeySenderSSRC]            = "SenderSSRC";
    m[KeySentNTP]               = "SentNTP";
    m[KeySentOctets]            = "SentOctets";
    m[KeySentPackets]           = "SentPackets";
    m[KeySentRTP]               = "SentRTP";
    m[KeySequenceNumberCycles]  = "SequenceNumberCycles";
    m[KeyServiceLevel]          = "ServiceLevel";
    m[KeyServicelevelPerf]      = "ServicelevelPerf";
    m[KeySIP_Callid]            = "SIP-Callid";
    m[KeySMS]                   = "SMS";
    m[KeySMSPDU]                = "SMSPDU";
    m[KeySMSServiceCenter]      = "SMSServiceCenter";
    m[KeyState]                 = "State";
    m[KeyStatus]                = "Status";
    m[KeyStrategy]              = "Strategy";
    m[KeySubEvent]              = "SubEvent";
    m[KeySubmode]               = "Submode";
    m[KeySubscriberNumber]      = "SubscriberNumber";
    m[KeyTalkTime]              = "TalkTime";
    m[KeyTargetChannel]         = "TargetChannel";
    m[KeyTargetUniqueid]        = "TargetUniqueid";
    m[KeyTasksInQueue]          = "TasksInQueue";
    m[KeyTextSupport]           = "TextSupport";
    m[KeyTimestamp]             = "Timestamp";
    m[KeyTheirLastSR]           = "TheirLastSR";
    m[KeyTo]                    = "To";
    m[KeyTransferContext]       = "TransferContext";
    m[KeyTransferExten]         = "TransferExten";
    m[KeyTransferMethod]        = "TransferMethod";
    m[KeyTransferType]          = "TransferType";
    m[KeyTXGain]                = "TXGain";
    m[KeyU2DIAG]                = "U2DIAG";
    m[KeyUniqueID]              = "UniqueID";
    m[KeyUniqueid]              = "Uniqueid";
    m[KeyUniqueid1]             = "Uniqueid1";
    m[KeyUniqueid2]             = "Uniqueid2";
    m[KeyUseCallingPres]        = "UseCallingPres";
    m[KeyUseUCS2Encoding]       = "UseUCS2Encoding";
    m[KeyUSSD]                  = "USSD";
    m[KeyUSSDUse7BitEncoding]   = "USSDUse7BitEncoding";
    m[KeyUSSDUseUCS2Decoding]   = "USSDUseUCS2Decoding";
    m[KeyUsername]              = "Username";
    m[KeyValue]                 = "Value";
    m[KeyVariable]              = "Variable";
    m[KeyVideoSupport]          = "VideoSupport";
    m[KeyVoice]                 = "Voice";
    m[KeyWaiting]               = "Waiting";
    m[KeyWeight]                = "Weight";
    m.squeeze();
    return m;
}

QMap<QString, AmiKey> initAmiStrKeys()
{
    QMap<QString, AmiKey> m;
    QHash<AmiKey, QString>::const_iterator i;
    for(i = AmiKeys.constBegin(); i != AmiKeys.constEnd(); ++i)
        m.insert(i.value(), i.key());
    return m;
}

QMap<QString, AstChannelState> initAstChannelStrStates()
{
    QMap<QString, AstChannelState> m;
    m["Down"]           = AstStateDown;
    m["Rsrvd"]          = AstStateReserved;
    m["OffHook"]        = AstStateOffhook;
    m["Dialing"]        = AstStateDialing;
    m["Ring"]           = AstStateRing;
    m["Ringing"]        = AstStateRinging;
    m["Up"]             = AstStateUp;
    m["Busy"]           = AstStateBusy;
    m["Dialing Offhook"] = AstStateDialingOffhook;
    m["Pre-ring"]       = AstStatePrering;
    return m;
}

const QHash<ActionType, QString> ActionTypes = initActionTypes();

const QMap<QString, ActionType> ActionStrTypes = initActionStrTypes();

const QHash<EventType, QString> EventTypes = initEventTypes();

const QMap<QString, EventType> EventStrTypes = initEventStrTypes();

const QHash<AmiKey, QString> AmiKeys = initAmiKeys();

const QMap<QString, AmiKey> AmiStrKeys = initAmiStrKeys();

const QMap<QString, AstChannelState> AstChannelStrStates = initAstChannelStrStates();

}
