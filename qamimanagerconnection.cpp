/*
 * Copyright © 2011-2012 CarelessChaser <chaser.andrey@gmail.com>
 *
 * This file is part of QtAmi
 *
 * QtAmi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QtAmi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QtAmi. If not, see <http://www.gnu.org/licenses/>.
 */
#include <sys/syslog.h>

#include "qamimanagerconnection.h"

const int DEFAULT_TIMEOUT = 2000;

static const char* status[] = { "Disconnected", "Connected", "Authenticated" };

using namespace QAmi;

QAmiManagerConnection::QAmiManagerConnection(QObject *parent) :
    QAmiResponseManager(parent), mState(DISCONNECTED), defaultActionTimeout(DEFAULT_TIMEOUT)
{
    connect(this, SIGNAL(sendAction(QAmiAction)), this, SLOT(slotSendAction(QAmiAction)));

    startSocketThread();
    qRegisterMetaType<bool*>("bool*");
}

void QAmiManagerConnection::startSocketThread()
{
    mReader = new QAmiReader;
    QThread * thread = new QThread(this);
    mReader->moveToThread(thread);
    connect(mReader, SIGNAL(finished()), thread, SLOT(quit()));
    connect(mReader, SIGNAL(finished()), thread, SLOT(deleteLater()));
    connect(mReader, SIGNAL(finished()), this, SLOT(slotDebug()), Qt::DirectConnection);
    connect(thread, SIGNAL(finished()), this, SLOT(slotDebug()), Qt::DirectConnection);
    connect(mReader, SIGNAL(finished()), mReader, SLOT(deleteLater()));
    connect(mReader, SIGNAL(newVersion(QByteArray)), this, SLOT(onAmiVersion(QByteArray)));
    connect(mReader, SIGNAL(newResponse(QByteArray)), this, SLOT(onResponse(QByteArray)), Qt::DirectConnection);
    connect(mReader, SIGNAL(newEvent(QByteArray)), this, SLOT(onNewEvent(QByteArray)));
    connect(mReader, SIGNAL(connected()), SLOT(onConnected()));
    connect(mReader, SIGNAL(disconnected()), SLOT(onDisconnected()));
    connect(mReader, SIGNAL(debugReceived(QByteArray)), SLOT(onDebugDataReceived(QByteArray)));
    thread->start();
}

void QAmiManagerConnection::connectToHost()
{
    QMetaObject::invokeMethod(mReader, "connectToHost", Qt::QueuedConnection,
                              Q_ARG(QString, m_Host),
                              Q_ARG(quint16, m_Port));
}

void QAmiManagerConnection::disconnectFromHost()
{
    mReader->disconnectFromHost();
    setState(DISCONNECTED);
}

void QAmiManagerConnection::onAuthenticated()
{
    mTimerPingRun.setInterval(10000);
    connect(&mTimerPingRun, SIGNAL(timeout()), this, SLOT(onPingerTimeout()));
    mTimerPingRun.start();
}

void QAmiManagerConnection::onPingerTimeout()
{
    QAmiAction action(AcPing);
    sendAction(action);
    mTimerPingTime.restart();
    mTimerPingRun.start();
}

bool QAmiManagerConnection::login()
{
    if(!this->isConnected()) {
        syslog(LOG_ERR, "Must be connect before login!");
        return false;
    } else if (this->isAuthenticated()) {
        syslog(LOG_WARNING, "Already logged in!");
        return true;
    }
    ActionLogin ac;
    ac.setUsername(m_Login);
    ac.setSecret(m_Password);
    slotSendAction(ac);
    return true;
}

void QAmiManagerConnection::onNewEvent(QByteArray evStr)
{
    QAmiEvent ev(evStr);
    switch(ev.type()) {
    case EvTypeAGIExec:
        emit evAGIExec(ev);
        break;
    case EvTypeAgentCalled:
        emit evAgentCalled(ev);
        break;
    case EvTypeAgentComplete:
        emit evAgentComplete(ev);
        break;
    case EvTypeAgentConnect:
        emit evAgentConnect(ev);
        break;
    case EvTypeAgentRingNoAnswer:
        emit evAgentRingNoAnswer(ev);
        break;
    case EvTypeBridge:
        emit evBridge(ev);
        break;
    case EvTypeChannelReload:
        emit evChannelReload(ev);
        break;
    case EvTypeChannelUpdate:
        emit evChannelUpdate(ev);
        break;
    case EvTypeCoreShowChannel:
        emit evCoreShowChannel(ev);
        break;
    case EvTypeCoreShowChannelsComplete:
        emit evCoreShowChannelsComplete(ev);
        break;
    case EvTypeDial:
        emit evDial(ev);
        break;
    case EvTypeDongleCallStateChange:
        emit evDongleCallStateChange(ev);
        break;
    case EvTypeDongleCEND:
        emit evDongleCEND(ev);
        break;
    case EvTypeDongleDeviceEntry:
        emit evDongleDeviceEntry(ev);
        break;
    case EvTypeDongleNewCMGR:
        emit evDongleNewCMGR(ev);
        break;
    case EvTypeDongleNewCUSD:
        emit evDongleNewCUSD(ev);
        break;
    case EvTypeDongleNewSMS:
        emit evDongleNewSMS(ev);
        break;
    case EvTypeDongleNewSMSBase64:
        emit evDongleNewSMSBase64(ev);
        break;
    case EvTypeDongleNewUSSD:
        emit evDongleNewUSSD(ev);
        break;
    case EvTypeDongleNewUSSDBase64:
        emit evDongleNewUSSDBase64(ev);
        break;
    case EvTypeDonglePortFail:
        emit evDonglePortFail(ev);
        break;
    case EvTypeDongleShowDevicesComplete:
        emit evDongleShowDevicesComplete(ev);
        break;
    case EvTypeDongleSMSStatus:
        emit evDongleSMSStatus(ev);
        break;
    case EvTypeDongleStatus:
        emit evDongleStatus(ev);
        break;
    case EvTypeDongleUSSDStatus:
        emit evDongleUSSDStatus(ev);
        break;
    case EvTypeDTMF:
        emit evDTMF(ev);
        break;
    case EvTypeFullyBooted:
        emit evFullyBooted(ev);
        break;
    case EvTypeJoin:
        emit evJoin(ev);
        break;
    case EvTypeHangup:
        emit evHangup(ev);
        break;
    case EvTypeLeave:
        emit evLeave(ev);
        break;
    case EvTypeMasquerade:
        emit evMasquerade(ev);
        break;
    case EvTypeMusicOnHold:
        emit evMusicOnHold(ev);
        break;
    case EvTypeNewAccountCode:
        emit evNewAccountCode(ev);
        break;
    case EvTypeNewCallerid:
        emit evNewCallerid(ev);
        break;
    case EvTypeNewchannel:
        emit evNewchannel(ev);
        break;
    case EvTypeNewexten:
        emit evNewexten(ev);
        break;
    case EvTypeNewstate:
        emit evNewstate(ev);
        break;
    case EvTypePeerEntry:
        emit evPeerEntry(ev);
        break;
    case EvTypePeerStatus:
        emit evPeerStatus(ev);
        break;
    case EvTypeQueueCallerAbandon:
        emit evQueueCallerAbandon(ev);
        break;
    case EvTypeQueueEntry:
        emit evQueueEntry(ev);
        break;
    case EvTypeQueueMember:
        emit evQueueMember(ev);
        break;
    case EvTypeQueueMemberAdded:
        emit evQueueMemberAdded(ev);
        break;
    case EvTypeQueueMemberPaused:
        emit evQueueMemberPaused(ev);
        break;
    case EvTypeQueueMemberRemoved:
        emit evQueueMemberRemoved(ev);
        break;
    case EvTypeQueueMemberStatus:
        emit evQueueMemberStatus(ev);
        break;
    case EvTypeQueueParams:
        emit evQueueParams(ev);
        break;
    case EvTypeQueueStatusComplete:
        emit evQueueStatusComplete(ev);
        break;
    case EvTypeRegistry:
        emit evRegistry(ev);
        break;
    case EvTypeRename:
        emit evRename(ev);
        break;
    case EvTypeRTCPReceived:
        emit evRTCPReceived(ev);
        break;
    case EvTypeRTCPSent:
        emit evRTCPSent(ev);
        break;
    case EvTypeShutdown:
        emit evShutdown(ev);
        break;
    case EvTypeStatus:
        emit evStatus(ev);
        break;
    case EvTypeStatusComplete:
        emit evStatusComplete(ev);
        break;
    case EvTypeTransfer:
        emit evTransfer(ev);
        break;
    case EvTypeUnlink:
        emit evUnlink(ev);
        break;
    case EvTypeVarSet:
        emit evVarSet(ev);
        break;
    default:
        syslog(LOG_WARNING, "AMI: No signal for event \"%s\"", qPrintable(ev.typeStr()));
        qWarning("AMI: No signal for event \"%s\"", qPrintable(ev.typeStr()));
        break;
    }
    emit amiEvent(ev);
}

void QAmiManagerConnection::setState(State newState)
{
    //if(mState == newState)
    //    return;
    syslog(LOG_NOTICE, "AMI: Status %s => %s", status[mState], status[newState]);
    qDebug("AMI: Status %s => %s", status[mState], status[newState]);
    switch(newState) {
    case DISCONNECTED:
        mState = newState;
        emit disconnected();
        break;
    case CONNECTED:
        mState = newState;
        emit connected();
        break;
    case AUTHENTICATED:
        mState = newState;
        onAuthenticated();
        break;
    }
}

bool QAmiManagerConnection::isConnected()
{
    return (this->mState == CONNECTED);
}

bool QAmiManagerConnection::isAuthenticated()
{
    return (this->mState == AUTHENTICATED);
}

bool QAmiManagerConnection::slotSendAction(QAmiAction action)
{
    if(mState == AUTHENTICATED || (mState == CONNECTED && action.type() == AcLogin)) {
        addAction(action);
        mReader->sendAction(action.toString());
        emit amiAction(action);
        return true;
    }
    qDebug("AMI: can't send action. Not authenticated. Action %s", qPrintable(action.toString()));
    syslog(LOG_WARNING, "AMI: can't send action. Not authenticated. Action %s", qPrintable(action.toString()));
    return false;
}

void QAmiManagerConnection::slotSocketThreadStarted()
{

}

void QAmiManagerConnection::slotDebug()
{
    qDebug() << "THREAD EXIT!!1";
}

void QAmiManagerConnection::onConnected()
{
    setState(CONNECTED);
}

void QAmiManagerConnection::onDisconnected()
{
    setState(DISCONNECTED);
}

void QAmiManagerConnection::onAmiVersion(QByteArray version)
{
    emit amiVersion(version);
}

void QAmiManagerConnection::onDebugDataReceived(QByteArray data)
{
    emit debugDataReceived(data);
}
