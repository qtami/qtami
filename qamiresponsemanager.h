/*
 * Copyright © 2011-2012 CarelessChaser <chaser.andrey@gmail.com>
 *
 * This file is part of QtAmi
 *
 * QtAmi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QtAmi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QtAmi. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef QAMIRESPONSEMANAGER_H
#define QAMIRESPONSEMANAGER_H

#include <QObject>
#include <QtCore/QHash>
#include <QtCore>

#include "qamiaction.h"
#include "qamiresponse.h"


namespace QAmi {

class QAmiResponseManager : public QThread
{
    Q_OBJECT
public:
    explicit QAmiResponseManager(QObject *parent = 0);
    ~QAmiResponseManager();
    void start();
    void stop();

protected:
    void addAction(const QAmiAction &action, quint32 timeout_msec = 60000);
    QTime mTimerPingTime;
private:
    QHash<QString, QAmiAction> mActions;
    QHash<QString, QTimer*> m_actionIdTimeout;
    QHash<QTimer*, QString> m_timeoutActionId;
    QHash<QString, QTime> m_timers;

signals:
    void sendAction(QAmiAction);
    void amiResponse(QAmiResponse, QAmiAction);
    void responseCoreShowChannels(QAmiResponse);
    void responseDongleSendSMS(ResponseDongleSendSMS, ActionDongleSendSMS);
    void responseDongleSendUSSD(ResponseDongleSendUSSD, ActionDongleSendUSSD);
    void responseDongleShowDevices(QAmiResponse);
    void responseGetvar(ResponseGetvar, ActionGetvar);
    void responseListCommands(QAmiResponse);
    void responseLogin(QAmiResponse);
    void responseOriginate(ResponseOriginate, ActionOriginate);
    void responsePing(QAmiResponse, int);
    void responseQueueAdd(QAmiResponse, QAmiAction);
    void responseQueuePause(ResponseQueuePause, ActionQueuePause);
    void responseQueueRemove(QAmiResponse, QAmiAction);
    void responseQueueStatus(QAmiResponse);
    void responseSetvar(ResponseSetvar, ActionSetvar);
    void responseSIPshowpeer(QAmiResponse);
    void responseStatus(QAmiResponse);


public slots:

protected slots:
    void onResponse(QByteArray str);
private slots:
    void onTimeout();
};

}

#endif // QAMIRESPONSEMANAGER_H
