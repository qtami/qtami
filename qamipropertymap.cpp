/*
 * Copyright © 2011-2012 CarelessChaser <chaser.andrey@gmail.com>
 *
 * This file is part of QtAmi
 *
 * QtAmi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QtAmi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QtAmi. If not, see <http://www.gnu.org/licenses/>.
 */
#include <QtCore/QDebug>
#include <QtCore/QStringBuilder>
#include <QtCore/QStringList>
#include "qamipropertymap.h"

const QString SEP = ": ";
const QString NEWLINE = "\r\n";

using namespace QAmi;

QAmiPropertyMap::QAmiPropertyMap()
{
}

QAmiPropertyMap::QAmiPropertyMap(const QByteArray &str)
{
    convertStr(str);
}

void QAmiPropertyMap::convertStr(const QByteArray &str)
{
    QStringList lines = QString(str).split(NEWLINE);
    for(const auto &s : lines) {
        int cutAt = s.indexOf(SEP);
        QString key = s.left(cutAt);
        QString value = s.right(s.length() - cutAt - SEP.length());
        m_Values.insert(key, value);
    }
}

QString QAmiPropertyMap::value(const QByteArray &key) const
{
    return m_Values.value(key);
}

QString QAmiPropertyMap::value(AmiKey key) const
{
    return m_Values.value(AmiKeys.value(key));
}

void QAmiPropertyMap::setValue(const QByteArray &key, const QByteArray &value)
{
    m_Values[key] = value;
}

void QAmiPropertyMap::setValue(const QByteArray &key, const QString &value)
{
    m_Values[key] = value;
}

void QAmiPropertyMap::setValue(AmiKey key, const QByteArray &value)
{
    m_Values[AmiKeys.value(key)] = value;
}

void QAmiPropertyMap::setValue(AmiKey key, const QString &value)
{
    m_Values[AmiKeys.value(key)] = value;
}

void QAmiPropertyMap::setMultiValue(AmiKey key, const QString &value)
{
    m_Values.insertMulti(AmiKeys.value(key), value);
}

QString QAmiPropertyMap::toString() const
{
    QString str;
    QMap<QString, QString>::const_iterator i = m_Values.constBegin();
    while (i != m_Values.constEnd()) {
        str.append(i.key()%": "%i.value()%"\r\n");
        ++i;
    }
    str.append("\r\n");
    return str;
}
