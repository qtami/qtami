/*
 * Copyright © 2011-2012 CarelessChaser <chaser.andrey@gmail.com>
 *
 * This file is part of QtAmi
 *
 * QtAmi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QtAmi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QtAmi. If not, see <http://www.gnu.org/licenses/>.
 */
#include "qamiaction.h"

#include <sys/syslog.h>
#include <QtCore/QStringBuilder>

#include "qamistructs.h"


static qint64 lastId = 0;

using namespace QAmi;

QAmiAction::QAmiAction()
{
    setActionId(generateId());
}

QAmiAction::QAmiAction(ActionType type)
{
    this->setActionType(type);
    setActionId(generateId());
}

QAmiAction::QAmiAction(const QAmiAction &action) :
    QAmiPropertyMap()
{
    setValues(action.values());
    setActionType(action.type());
    setActionId(action.actionId());
}

QAmiAction::~QAmiAction()
{
}

QString QAmiAction::actionId() const
{
    return value(KeyActionID);
}

void QAmiAction::setActionId(const QString &str)
{
    setValue(KeyActionID, str);
}

QString QAmiAction::name() const
{
    if(!ActionTypes.contains(mType)) {
        syslog(LOG_WARNING, "%s: No action in ActionTypes", Q_FUNC_INFO);
        qCritical("%s: No action in ActionTypes", Q_FUNC_INFO);
    }
    return ActionTypes[mType];
}

ActionType QAmiAction::type() const
{
    return mType;
}

void QAmiAction::setActionType(ActionType type)
{
    mType = type;
}

const QString QAmiAction::generateId()
{
    return QString::number(++lastId);;
}

QString QAmiAction::toString() const
{
    QString str = "Action: " % name() % "\r\n" %
        QAmiPropertyMap::toString();
    return str;

}

