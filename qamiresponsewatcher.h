/*
 * Copyright © 2011-2012 CarelessChaser <chaser.andrey@gmail.com>
 *
 * This file is part of QtAmi
 *
 * QtAmi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QtAmi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QtAmi. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef QAMIRESPONSEWATCHER_H
#define QAMIRESPONSEWATCHER_H

#include <QtCore/QObject>

#include "qamiaction.h"
#include "qamievent.h"

namespace QAmi {

class QAmiResponseWatcher : public QObject
{
    Q_OBJECT
public:
    enum Type {
        AgentChannels
    };

    explicit QAmiResponseWatcher(QObject *parent = 0);
    explicit QAmiResponseWatcher(const QAmiAction &action, QObject *parent = 0);

    void trigger();
    QAmiAction action();
    QString actionId();
    void addEvent(QAmiEvent);
    const QAmiEvent lastEvent();
    const QList<QAmiEvent>& events();
    void setType(Type type);
    Type type();
private:
    QAmiAction mAction;
    QList<QAmiEvent> mEvents;
    Type m_Type;

signals:
    void triggered();


public slots:



};
}

#endif // QAMIRESPONSEWATCHER_H
