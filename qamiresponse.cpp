/*
 * Copyright © 2011-2012 CarelessChaser <chaser.andrey@gmail.com>
 *
 * This file is part of QtAmi
 *
 * QtAmi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QtAmi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QtAmi. If not, see <http://www.gnu.org/licenses/>.
 */
#include <QtCore/QDebug>

#include "qamiresponse.h"

#define TYPE_SUCCESS "Success"
#define TYPE_ERROR "Error"

using namespace QAmi;

QAmiResponse::QAmiResponse(const QAmiResponse &response) :
    QAmiPropertyMap()
{
    setValues(response.values());
}

QAmiResponse::QAmiResponse(const QByteArray &str) :
    QAmiPropertyMap(str)
{
}

QAmiResponse::~QAmiResponse()
{
}

QString QAmiResponse::actionId() const
{
    return value(KeyActionID);
}

bool QAmiResponse::isSuccess() const
{
    if(this->value("Response") == TYPE_SUCCESS)
        return true;
    return false;
}
