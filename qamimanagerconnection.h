/*
 * Copyright © 2011-2012 CarelessChaser <chaser.andrey@gmail.com>
 *
 * This file is part of QtAmi
 *
 * QtAmi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QtAmi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QtAmi. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef QAMIMANAGERCONNECTION_H
#define QAMIMANAGERCONNECTION_H

#include <QtCore/QObject>
#include <QtNetwork/QTcpSocket>

#include "qamievent.h"
#include "qamireader.h"
#include "qamiresponsemanager.h"



namespace QAmi {

class QAmiManagerConnection : public QAmiResponseManager
{
    Q_OBJECT
public:
    enum State { DISCONNECTED = 0, CONNECTED = 1, AUTHENTICATED = 2 };
    explicit QAmiManagerConnection(QObject *parent = 0);
    void connectToHost();
    void disconnectFromHost();
    bool login();
    bool isConnected();
    bool isAuthenticated();

    void setState(State newState);
    inline void setLogin(const QString &login) { m_Login = login; }
    inline void setPassword(const QString &password) { m_Password = password; }
    inline void setHost(const QString &host) { m_Host = host; }
    inline void setPort(int port) { m_Port = port; }

private:
    State mState;
    quint32 defaultActionTimeout;
    QAmiReader *mReader;
    QString m_Login;
    QString m_Password;
    QString m_Host;
    quint16 m_Port;

    QTimer mTimerPingRun;


    void startSocketThread();
    void onAuthenticated();
signals:
    void amiAction(QAmiAction);
    void amiEvent(QAmiEvent);
    void amiVersion(QString);
    void connected();
    void disconnected();
    void evAGIExec(QAmiEvent);
    void evAgentCalled(QAmiEvent);
    void evAgentComplete(QAmiEvent);
    void evAgentConnect(QAmiEvent);
    void evAgentRingNoAnswer(QAmiEvent);
    void evBridge(EventBridge);
    void evChannelReload(QAmiEvent);
    void evChannelUpdate(QAmiEvent);
    void evCoreShowChannel(EventCoreShowChannel);
    void evCoreShowChannelsComplete(QAmiEvent);
    void evDial(QAmiEvent);
    void evDongleCallStateChange(QAmiEvent);
    void evDongleCEND(QAmiEvent);
    void evDongleDeviceEntry(EventDongleDeviceEntry);
    void evDongleNewCMGR(QAmiEvent);
    void evDongleNewCUSD(QAmiEvent);
    void evDongleNewSMS(QAmiEvent);
    void evDongleNewSMSBase64(QAmiEvent);
    void evDongleNewUSSD(QAmiEvent);
    void evDongleNewUSSDBase64(EventDongleNewUSSDBase64);
    void evDonglePortFail(QAmiEvent);
    void evDongleShowDevicesComplete(QAmiEvent);
    void evDongleSMSStatus(EventDongleSMSStatus);
    void evDongleStatus(EventDongleStatus);
    void evDongleUSSDStatus(QAmiEvent);
    void evDTMF(QAmiEvent);
    void evFullyBooted(QAmiEvent);
    void evJoin(QAmiEvent);
    void evHangup(EventHangup);
    void evLeave(QAmiEvent);
    void evMasquerade(QAmiEvent);
    void evMusicOnHold(QAmiEvent);
    void evNewAccountCode(QAmiEvent);
    void evNewCallerid(QAmiEvent);
    void evNewchannel(EventNewchannel);
    void evNewexten(QAmiEvent);
    void evNewstate(EventNewstate);
    void evPeerEntry(EventPeerEntry);
    void evPeerStatus(EventPeerStatus);
    void evQueueCallerAbandon(QAmiEvent);
    void evQueueEntry(QAmiEvent);
    void evQueueMember(EventQueueMember);
    void evQueueMemberAdded(EventQueueMemberAdded);
    void evQueueMemberPaused(EventQueueMemberPaused);
    void evQueueMemberRemoved(EventQueueMemberRemoved);
    void evQueueMemberStatus(EventQueueMemberStatus);
    void evQueueParams(QAmiEvent);
    void evQueueStatusComplete(QAmiEvent);
    void evRegistry(QAmiEvent);
    void evRename(EventRename);
    void evRTCPReceived(QAmiEvent);
    void evRTCPSent(QAmiEvent);
    void evShutdown(QAmiEvent);
    void evStatus(EventStatus);
    void evStatusComplete(QAmiEvent);
    void evTransfer(QAmiEvent);
    void evUnlink(QAmiEvent);
    void evVarSet(QAmiEvent);

    void debugDataReceived(QByteArray data);

private slots:

    void onNewEvent(QByteArray evStr);
    void slotSocketThreadStarted();
    void onConnected();
    void slotDebug();
    void onPingerTimeout();
    void onDisconnected();
    void onAmiVersion(QByteArray version);
    void onDebugDataReceived(QByteArray data);
public slots:
    bool slotSendAction(QAmiAction action);

};

}

#endif // QAMIMANAGERCONNECTION_H
