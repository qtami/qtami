/*
 * Copyright © 2011-2012 CarelessChaser <chaser.andrey@gmail.com>
 *
 * This file is part of QtAmi
 *
 * QtAmi is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QtAmi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QtAmi. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef QAMIPROPERTYMAP_H
#define QAMIPROPERTYMAP_H

#include <QtCore/QMap>
#include <QtCore/QString>
#include "qamistructs.h"

namespace QAmi {

class QAmiPropertyMap
{
public:
    explicit QAmiPropertyMap();
    explicit QAmiPropertyMap(const QByteArray &str);
    QString value(const QByteArray &key) const;
    QString value(AmiKey key) const;
    virtual QString toString() const;
protected:
    inline bool contains(const QByteArray &key) const { return m_Values.contains(key); }
    inline bool contains(AmiKey key) const { return m_Values.contains(AmiKeys.value(key)); }
    void setValue(const QByteArray &key, const QByteArray &value);
    void setValue(const QByteArray &key, const QString &value);
    inline void setValue(const QByteArray &key, const char* value) { setValue(key, QString(value)); }
    void setValue(AmiKey key, const QByteArray &value);
    void setValue(AmiKey key, const QString &value);
    void setMultiValue(AmiKey key, const QString &value);
    inline void setValue(AmiKey key, const char* value) { setValue(key, QString(value)); }
    void convertStr(const QByteArray &str);
    inline QMap<QString, QString> values() const { return m_Values; }
    inline void setValues(QMap<QString, QString> values) { m_Values = values; }
private:
    // using QMap, because QMap is faster than QHash for strings at 10-100 items
    QMap<QString, QString> m_Values;
};

}

#endif // QAMIPROPERTYMAP_H
